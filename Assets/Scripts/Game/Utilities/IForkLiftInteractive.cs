﻿using Game.Entities;

namespace Game.Utilities
{
    public interface IForkLiftInteractive
    {
        void Interact(ForkLiftController forkLift);
    }
}