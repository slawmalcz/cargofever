﻿using System;
using UnityEngine;

namespace Game.Utilities
{
    [RequireComponent(typeof(Rigidbody))]
    [Obsolete("Not used any more. Delete when prefabs clear", true)]
    class PlayerImmovableObjects : MonoBehaviour
    {
        public void MakeStatic()
        {
            var affectedRigidbody = gameObject.GetComponent<Rigidbody>();
            affectedRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }

        public void MakeDynamic()
        {
            var affectedRigidbody = gameObject.GetComponent<Rigidbody>();
            affectedRigidbody.constraints = RigidbodyConstraints.None;
        }
    }
}
