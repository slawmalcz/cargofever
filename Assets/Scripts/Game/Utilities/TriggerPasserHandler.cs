﻿using System;
using UnityEngine;


namespace Game.Utilities
{
    [RequireComponent(typeof(ITagDetector))]
    public class TriggerPasserHandler : MonoBehaviour
    {
        public TriggerPasser[] detectors = null;
        public string detectedTag = "Player";

        private ITagDetector _IDetectableParent = null;
        private ITagDetector IDetectableParent {
            get {
                if (_IDetectableParent == null)
                    _IDetectableParent = GetComponent<ITagDetector>();
                return _IDetectableParent;
            }
        }

        /// <summary>
        /// Other, detector, isEnter
        /// </summary>
        private Action<GameObject, GameObject, bool> DetectionEvent => IDetectableParent.TriggerHandlerEvent;

        public void OnTriggersEnter(GameObject other, GameObject detector)
        {
            if (other.CompareTag(detectedTag))
                DetectionEvent.Invoke(other, detector, true);
        }
        public void OnTriggersExit(GameObject other, GameObject detector)
        {
            if (other.CompareTag(detectedTag))
                DetectionEvent.Invoke(other, detector, false);
        }
    }
}