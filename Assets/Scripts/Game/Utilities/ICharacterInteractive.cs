﻿using Game.Entities;
using UnityEngine;

namespace Game.Utilities
{
    public interface ICharacterInteractive
    {
        GameObject AttachedGameObject { get; }
        bool Interact(PlayerController player);
    }
}
