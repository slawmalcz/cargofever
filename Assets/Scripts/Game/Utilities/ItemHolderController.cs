﻿using Game.Entities;
using UnityEngine;

namespace Game.Utilities
{
    class ItemHolderController : MonoBehaviour, ICharacterInteractive
    {
        [Header("RequireComponents")]
        public Transform slotTransform = null;
        public Transform interactionTransform = null;

        public GameObject AttachedGameObject => gameObject;

        public bool Interact(PlayerController player)
        {
            if (slotTransform.childCount == 0 && player.IsCarryObject)
            {
                ForcePosition(player);
                Instantiate(player.PutDown().prefab, slotTransform);
            }
            else if (slotTransform.childCount > 0 && !player.IsCarryObject)
            {
                ForcePosition(player);
                player.PickUp(slotTransform.GetChild(0).GetComponent<ItemController>().item);
                Destroy(slotTransform.GetChild(0).gameObject);
            }
            return true;
        }

        private void ForcePosition(PlayerController player)
        {
            player.transform.position = interactionTransform.transform.position;
            player.transform.rotation = interactionTransform.transform.rotation;
        }
    }
}
