﻿using System.Collections;
using UnityEngine;

namespace Game.Utilities
{
    public class RespawnOnTilt : MonoBehaviour
    {
        public float angle = 30;
        public float awaitTime = 3;
        private bool isTriggered = false;

        private void Update()
        {
            if (CalculateAngleDeviation(gameObject.transform.up, Vector3.up) > angle && !isTriggered)
                StartCoroutine(RestartTransform(awaitTime));
        }

        public float CalculateAngleDeviation(Vector3 origin, Vector3 deviation)
        {
            var originNorm = Mathf.Sqrt(Mathf.Pow(origin.x, 2) + Mathf.Pow(origin.y, 2) + Mathf.Pow(origin.z, 2));
            var deviationNorm = Mathf.Sqrt(Mathf.Pow(deviation.x, 2) + Mathf.Pow(deviation.y, 2) + Mathf.Pow(deviation.z, 2));
            var dotProduct = Vector3.Dot(origin, deviation);
            var conOfAngle = dotProduct / (originNorm * deviationNorm);
            return Mathf.Rad2Deg * Mathf.Acos(conOfAngle);
        }

        private IEnumerator RestartTransform(float time)
        {
            isTriggered = true;
            yield return new WaitForSeconds(time);
            gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1, gameObject.transform.position.z);
            gameObject.transform.rotation = Quaternion.Euler(0, gameObject.transform.rotation.eulerAngles.y, 0);
            isTriggered = false;
        }

    }
}

