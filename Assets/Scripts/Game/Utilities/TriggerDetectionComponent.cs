﻿using System;
using UnityEngine;

public class TriggerDetectionComponent : MonoBehaviour
{
    /// <summary>
    /// other, isEnter
    /// </summary>
    public event Action<GameObject, bool> DetectionEvent = delegate { };
    public Type detectionType = null;

    private void OnTriggerEnter(Collider other)
    {
        if (detectionType == null)
            return;
        if (other.gameObject.GetComponent(detectionType) != null)
            DetectionEvent.Invoke(other.gameObject, true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (detectionType == null)
            return;
        if (other.gameObject.GetComponent(detectionType) != null)
            DetectionEvent.Invoke(other.gameObject, false);
    }
}
