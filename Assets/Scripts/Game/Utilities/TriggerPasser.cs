﻿using UnityEngine;

namespace Game.Utilities
{
    [RequireComponent(typeof(Collider))]
    public class TriggerPasser : MonoBehaviour
    {
        public TriggerPasserHandler triggerHandler = null;

        private void OnTriggerEnter(Collider other) => triggerHandler.OnTriggersEnter(other.gameObject, gameObject);
        private void OnTriggerExit(Collider other) => triggerHandler.OnTriggersExit(other.gameObject, gameObject);
    }
}
