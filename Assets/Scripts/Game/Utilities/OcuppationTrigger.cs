﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class OcuppationTrigger : MonoBehaviour
{
    /// <summary>
    /// other, isEnter
    /// </summary>
    public bool IsFree => currentOcupatorsObjets.Count == 0;

    private List<GameObject> currentOcupatorsObjets;

    public void Start()
    {
        currentOcupatorsObjets = new List<GameObject>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!currentOcupatorsObjets.Contains(other.gameObject))
            currentOcupatorsObjets.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentOcupatorsObjets.Contains(other.gameObject))
            currentOcupatorsObjets.Remove(other.gameObject);
    }
}