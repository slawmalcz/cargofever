﻿using Game.Entities;
using UnityEngine;

namespace Game.Utilities
{
    [RequireComponent(typeof(TriggerDetectionComponent))]
    public class CharacterInteractiveController : MonoBehaviour
    {
        public TriggerDetectionComponent interactionZone = null;

        public GameObject activationParent = null;

        public void Start()
        {
            interactionZone.detectionType = typeof(PlayerController);
            interactionZone.DetectionEvent +=
                (GameObject playerControllerGameObject, bool isEntering) =>
                {
                    playerControllerGameObject.GetComponent<PlayerController>().ApplyInteractive(activationParent, isEntering);
                };
        }
    }
}
