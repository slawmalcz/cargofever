﻿using Game.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Utilities
{
    public interface ICharacterHoldInteractive
    {
        GameObject AttachedGameObject { get; }
        bool HoldInteractBegine(PlayerController player);
        bool HoldInteractEnd(PlayerController player);
    }
}
