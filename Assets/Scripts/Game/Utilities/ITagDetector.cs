﻿using UnityEngine;

namespace Game.Utilities
{
    public interface ITagDetector
    {
        TriggerPasserHandler TriggerHandler { get; }

        void TriggerHandlerEvent(GameObject other, GameObject detector, bool isEnter);
    }
}
