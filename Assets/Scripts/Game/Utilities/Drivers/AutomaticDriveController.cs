﻿using System;
using UnityEngine;

namespace Game.Utilities
{
    public class AutomaticDriveController : AbstractDriverController
    {
        [Header("Required components")]
        public Rigidbody vechicleRigidbody = null;
        public LorryController lorryController = null;
        [Header("Vechicle options")]
        public float maxSpeed = 2;
        public float maxWhelleRotation = 45;
        public float maxMotorTorque = 400;
        public float breakTorque = 300;
        public float decelerationDistance = 1f;
        [Range(0, 1)]
        public float engineBreakForce = 0.5f;
        [Header("Destination options")]
        public float destinationReachedTreshold = 1f;
        public bool IsDuringTransfer { get; private set; } = false;
        public bool IsDestinationReached => Vector3.Distance(transform.position, destination) < destinationReachedTreshold;

        [HideInInspector]
        public Vector3 destination = Vector3.zero;
        private Vector2 TargetVector => new Vector2(destination.x, destination.z) - new Vector2(transform.position.x, transform.position.z);

        private DriveParameters currentParameters = new DriveParameters();

        public event Action OnDestinationReached = delegate { };

        internal void StartTransfer()
        {
            IsDuringTransfer = true;
            lorryController?.StartTransfer();
        }

        internal void StopTransfer()
        {
            IsDuringTransfer = false;
            lorryController?.StopTransfer();
            OnDestinationReached.Invoke();
        }

        private void ManageMotors()
        {
            if (!IsDuringTransfer)
                return;

            var unclamptAngle = Vector2.SignedAngle(TargetVector, new Vector2(transform.forward.x, transform.forward.z));
            if (unclamptAngle > 90 || unclamptAngle < -90)
                currentParameters.motorTorque = maxMotorTorque * Mathf.Clamp((Vector3.Distance(transform.position, destination) - decelerationDistance * engineBreakForce) / decelerationDistance, 1, -1);
            else
                currentParameters.motorTorque = maxMotorTorque * Mathf.Clamp((Vector3.Distance(transform.position, destination) - decelerationDistance * engineBreakForce) / decelerationDistance, -1, 1);

        }

        private void ManageRotation()
        {
            if (!IsDuringTransfer)
                return;
            var unclamptAngle = Vector2.SignedAngle(TargetVector, new Vector2(transform.forward.x, transform.forward.z));
            if (unclamptAngle > 90 || unclamptAngle < -90)
            {
                //Need of reverse
                if (lorryController != null)
                    lorryController.FreezeJoint(transform.rotation);
                unclamptAngle = -Vector2.SignedAngle(TargetVector, new Vector2(-transform.forward.x, -transform.forward.z));
                currentParameters.steerAngle = Mathf.Clamp(unclamptAngle, -maxWhelleRotation, maxWhelleRotation);
            }
            else
            {
                //Foward pursuit
                currentParameters.steerAngle = Mathf.Clamp(unclamptAngle, -maxWhelleRotation, maxWhelleRotation);
            }
        }

        private void ManageBreaks()
        {
            if (!IsDuringTransfer || Vector2.Dot(new Vector2(vechicleRigidbody.velocity.x, vechicleRigidbody.velocity.z), TargetVector) < -0.05)
                currentParameters.breakTorque = breakTorque;
            else
                currentParameters.breakTorque = 0;
        }

        private void ManageSpeed()
        {
            if (vechicleRigidbody.velocity.magnitude > maxSpeed)
                currentParameters.motorTorque = 0;

        }

        protected override DriveParameters CreateAxleParameters()
        {
            ManageBreaks();
            ManageMotors();
            ManageRotation();
            ManageSpeed();
            return currentParameters;
        }
    }
}
