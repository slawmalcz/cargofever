﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Utilities
{
    public class AutomaticDriveSpineLine : MonoBehaviour
    {
        public Vector3[] ControlPointsPositions {
            get {
                if(_positions == null)
                {
                    var childs = new List<Vector3>();
                    for (var i = 0; i < gameObject.transform.childCount; i++)
                        childs.Add(gameObject.transform.GetChild(i).position);
                    _positions = childs.ToArray();
                }
                return _positions;
            }
        }
        private Vector3[] _positions = null;

        public void StartTravel(AutomaticDriveController automaticDrive) => StartCoroutine(Travel(automaticDrive));

        IEnumerator Travel(AutomaticDriveController automaticDrive)
        {
            var currentDestination = 0;
            if (automaticDrive.IsDuringTransfer)
                throw new Exception("Can't start if is occupied");
            automaticDrive.StartTransfer();
            while (currentDestination < ControlPointsPositions.Length)
            {
                automaticDrive.destination = ControlPointsPositions[currentDestination];
                yield return new WaitWhile(() => { return !automaticDrive.IsDestinationReached; });
                currentDestination++;
            }
            automaticDrive.StopTransfer();
        }
    }
}
