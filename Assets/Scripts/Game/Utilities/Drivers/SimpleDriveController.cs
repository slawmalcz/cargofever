﻿using System;
using UnityEngine;

namespace Game.Utilities
{
    //[Obsolete("This class is only for test use")]
    public class SimpleDriveController : AbstractDriverController
    {
        public float maxMotorTorque;
        public float maxSteeringAngle;
        public float breakTorque;
        public float maxSpeed = 2;
        public Rigidbody vechicleRigidbody = null;

        protected override DriveParameters CreateAxleParameters() => new DriveParameters()
        {
            breakTorque = (isActive) ? breakTorque * (1 - Mathf.Abs(Input.GetAxis("Vertical"))) : breakTorque,
            steerAngle = (isActive) ? maxSteeringAngle * Input.GetAxis("Horizontal") : 0,
            motorTorque = (isActive && vechicleRigidbody.velocity.magnitude < maxSpeed) ? maxMotorTorque * Input.GetAxis("Vertical") : 0
        };
    }
}
