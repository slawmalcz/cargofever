﻿using UnityEngine;

namespace Game.Utilities
{
    class ForkLiftDriverController : AbstractDriverController
    {
        [Header("Required component")]
        public Rigidbody vechicleRigidbody = null;
        [Header("Diving properties")]
        public float maxMotorTorque = 400;
        public float maxSteeringAngle = 30;
        public float maxSpeed = 2;

        private DriveParameters currentParameters = new DriveParameters();

        protected override DriveParameters CreateAxleParameters()
        {
            if (isActive)
            {
                currentParameters.breakTorque = 0;
                currentParameters.steerAngle = maxSteeringAngle * Input.GetAxis("Horizontal");
                currentParameters.motorTorque = (vechicleRigidbody.velocity.magnitude <= maxSpeed) ? maxMotorTorque * Input.GetAxis("Vertical") : 0;
            }
            else
            {
                currentParameters.breakTorque = 1;
                currentParameters.motorTorque = 0;
            }
            return currentParameters;
        }
    }
}
