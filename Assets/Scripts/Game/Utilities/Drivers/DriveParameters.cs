﻿namespace Game.Utilities
{
    public struct DriveParameters
    {
        public float breakTorque;
        public float steerAngle;
        public float motorTorque;
    }
}
