﻿using UnityEngine;

namespace Game.Utilities
{
    public abstract class AbstractDriverController : MonoBehaviour
    {
        [HideInInspector]
        public bool isActive = true;

        [HideInInspector]
        public AxleData[] axles;

        protected abstract DriveParameters CreateAxleParameters();

        public void FixedUpdate()
        {
            var parameters = new DriveParameters();
            if (isActive)
                parameters = CreateAxleParameters();
            for (var i = 0; i < axles.Length; i++)
            {
                var axleInfo = axles[i];
                axleInfo.Apply(parameters);
            }
        }
    }
}
