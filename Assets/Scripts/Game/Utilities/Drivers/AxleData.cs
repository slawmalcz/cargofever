﻿using System;
using UnityEngine;

namespace Game.Utilities
{
    [Serializable]
    public class AxleData
    {
        public WheelCollider leftWheel;
        public GameObject leftWheelModel;
        public WheelCollider rightWheel;
        public GameObject rightWheelModel;
        public bool motor;
        public bool steering;

        public void Apply(DriveParameters parameters)
        {
            //Wheels breaks
            leftWheel.brakeTorque = parameters.breakTorque;
            rightWheel.brakeTorque = parameters.breakTorque;
            //Wheels turn
            if (steering)
            {
                leftWheel.steerAngle = parameters.steerAngle;
                rightWheel.steerAngle = parameters.steerAngle;
            }
            //Wheels acceleration
            if (motor)
            {
                leftWheel.motorTorque = parameters.motorTorque;
                rightWheel.motorTorque = parameters.motorTorque;
            }

            ApplyLocalPositionToVisuals(leftWheel, leftWheelModel);
            ApplyLocalPositionToVisuals(rightWheel, rightWheelModel);
        }

        private void ApplyLocalPositionToVisuals(WheelCollider collider, GameObject model)
        {
            Vector3 position;
            Quaternion rotation;
            collider.GetWorldPose(out position, out rotation);

            model.transform.position = position;
            model.transform.rotation = rotation;
        }
    }
}
