﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Utilities
{
    [RequireComponent(typeof(Rigidbody))]
    public class LorryController : AbstractDriverController
    {
        [Header("Required Components")]
        public Rigidbody lorryRigidbody = null;

        public float rotationFreezeDeley = 3f;

        protected override DriveParameters CreateAxleParameters() => new DriveParameters() { breakTorque = 0, motorTorque = 0, steerAngle = 0 };

        public void StartTransfer()
        {
            StopAllCoroutines();
            lorryRigidbody.constraints = RigidbodyConstraints.None;
        }

        public void StopTransfer() => StartCoroutine(DeleyedStop(rotationFreezeDeley));


        private IEnumerator DeleyedStop(float time)
        {
            yield return new WaitForSeconds(time);
            lorryRigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }

        public void FreezeJoint(Quaternion freezedRotation)
        {
            gameObject.transform.rotation = freezedRotation;
        }
    }
}
