﻿using System.Collections;
using UnityEngine;

namespace Game.Utilities
{
    class AngleDoorController : AbstractDoorController
    {
        public float agleOfOpening = 90;
        public bool isRight = false;
        public Transform pivot = null;

        public override void Open()
        {
            if (currentCorutine != null)
                StopCoroutine(currentCorutine);
            currentCorutine = StartCoroutine(Interpolate((isRight) ? agleOfOpening : -agleOfOpening, actionTime));
        }
        public override void Close()
        {
            if (currentCorutine != null)
                StopCoroutine(currentCorutine);
            currentCorutine = StartCoroutine(Interpolate((isRight) ? -agleOfOpening : agleOfOpening, actionTime));
        }

        private IEnumerator Interpolate(float destination, float timeToPerform)
        {
            var timePassed = 0f;
            while (timePassed < timeToPerform)
            {
                var timePrecentage = Time.deltaTime / timeToPerform;
                gameObject.transform.RotateAround(pivot.position, Vector3.up, destination * timePrecentage);
                yield return new WaitForFixedUpdate();
                timePassed += Time.deltaTime;
            }
        }
    }
}
