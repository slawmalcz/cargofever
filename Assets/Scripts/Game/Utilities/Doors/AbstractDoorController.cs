﻿using UnityEngine;

namespace Game.Utilities
{
    public abstract class AbstractDoorController : MonoBehaviour
    {
        public float actionTime = 1f;
        protected Coroutine currentCorutine = null;

        public abstract void Open();
        public abstract void Close();
    }
}
