﻿using System.Collections;
using UnityEngine;

namespace Game.Utilities
{
    class SlidingDoorController : AbstractDoorController
    {
        public Transform openPosition = null;
        public Transform closePosition = null;

        private float PointDistance => Vector3.Distance(openPosition.position, closePosition.position);

        public override void Open()
        {
            if (currentCorutine != null)
                StopCoroutine(currentCorutine);
            currentCorutine = StartCoroutine(Interpolate(gameObject.transform.position,openPosition.position));
        }
        public override void Close()
        {
            if (currentCorutine != null)
                StopCoroutine(currentCorutine);
            currentCorutine = StartCoroutine(Interpolate(gameObject.transform.position, closePosition.position));
        }

        private IEnumerator Interpolate(Vector3 startPosition, Vector3 destination)
        {
            var timeToPerform = actionTime * Vector3.Distance(startPosition, destination) / PointDistance;
            var timePassed = 0f;
            while (timePassed < timeToPerform)
            {
                var timePrecentage = timePassed / timeToPerform;
                gameObject.transform.position = Vector3.Lerp(startPosition, destination, timePrecentage);
                yield return new WaitForFixedUpdate();
                timePassed += Time.deltaTime;
            }
        }
    }
}
