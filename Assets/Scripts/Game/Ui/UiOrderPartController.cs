﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    class UiOrderPartController : MonoBehaviour
    {
        public Image itemPartImage = null;
        public Text itemPartDescription = null;

        internal void Init(Sprite itemSprite, string itemName, int itemQuantity)
        {
            itemPartImage.sprite = itemSprite;
            itemPartDescription.text = $"{itemName} X {itemQuantity}";
        }
    }
}
