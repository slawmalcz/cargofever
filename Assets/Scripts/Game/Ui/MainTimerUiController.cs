﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    public class MainTimerUiController : MonoBehaviour
    {
        private bool IsReady = false;
        private float timeLeft = 0;
        private float fullTime = 0;
        private float PercentageOfTimeLeft => 1 - (timeLeft / fullTime);
        private Action roundEndCaller = null;
        private string TimeDisplay {
            get {
                var min = Mathf.FloorToInt(timeLeft / 60);
                var sec = Mathf.FloorToInt(timeLeft - min * 60);
                var displaySec = (sec < 10)? $"0{sec}":$"{sec}";
                var displayMin = (min < 10) ? $"0{min}" : $"{min}";
                return $"{displayMin}:{displaySec}";
            }
        }

        public Text textDisplay = null;
        public Gradient colorGradient = null;
        public Image colorDisplay = null;

        public void Init(float roundTime,Action roundEndCaller)
        {
            this.roundEndCaller = roundEndCaller;
            fullTime = roundTime;
            timeLeft = roundTime;
            colorDisplay.color = colorGradient.Evaluate(1);
            IsReady = true;
        }

        public void Update()
        {
            if (IsReady)
            {
                timeLeft -= Time.deltaTime;
                if(timeLeft <= 0)
                {
                    textDisplay.text = "0";
                    IsReady = false;
                    roundEndCaller.Invoke();
                    return;
                }
                textDisplay.text = $"{TimeDisplay}";
                colorDisplay.color = colorGradient.Evaluate(PercentageOfTimeLeft);
            }
        }

        public void StopTimer() => IsReady = false;
        public void StartTimer() => IsReady = true;

    }
}
