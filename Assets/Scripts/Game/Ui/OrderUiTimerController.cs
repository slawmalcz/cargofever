﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    public class OrderUiTimerController : MonoBehaviour
    {
        public Gradient deliveryColors = null;

        public Slider slider = null;
        public Image filling = null;
        public Text display = null;

        internal void UpdateSlider(float timePastPercentage, float timeLeft)
        {
            filling.color = deliveryColors.Evaluate(timePastPercentage);
            slider.value = timePastPercentage;
            display.text = TimeDisplay(timeLeft);
        }

        private string TimeDisplay(float timeleft)
        {
            var min = Mathf.FloorToInt(timeleft / 60);
            var sec = Mathf.FloorToInt(timeleft - min * 60);
            var displaySec = (sec < 10) ? $"0{sec}" : $"{sec}";
            var displayMin = (min < 10) ? $"0{min}" : $"{min}";
            return $"{displayMin}:{displaySec}";
        }
    }
}

