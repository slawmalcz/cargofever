﻿using Game.Data;
using UnityEngine;

namespace Game.Ui
{
    public class OrdersUiController : MonoBehaviour
    {
        public OrderUiController[] ordersSlots = null;

        public void Update()
        {
            foreach (var orderSlot in ordersSlots)
            {
                if (orderSlot.IsSlotActive)
                    orderSlot.contract.TickContract();
            }
        }

        public void AddContract(Contract contract)
        {
            foreach(var orderSlot in ordersSlots)
            {
                if (!orderSlot.IsSlotActive)
                {
                    orderSlot.Init(contract);
                    contract.OnTerminateEvent += TerminateContractUi;
                    break;
                }
            }
        }

        private void TerminateContractUi(Contract contract)
        {
            foreach (var orderSlot in ordersSlots)
            {
                if (orderSlot.contract == contract)
                {
                    orderSlot.IsSlotActive = false;
                    break;
                }
            }
        }
    }
}
