﻿using Game.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    public class UiMainScoreController : MonoBehaviour
    {
        public WorldController worldController = null;
        public Text scoreDisplay = null;

        private void Update() => scoreDisplay.text = worldController.points.ToString();
    }
}

