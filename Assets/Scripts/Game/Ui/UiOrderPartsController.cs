﻿using Game.Data;
using Game.Entities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    public class UiOrderPartsController : MonoBehaviour
    {
        public Text OrderName = null;

        public GameObject orderPartPrefab = null;
        public GameObject[] orderPartsSlots = null;

        public void Init(string orderName, Manifest manifest)
        {
            OrderName.text = orderName;
            foreach (var itemStack in manifest.ItemStacks)
                SpawnOrderElement(itemStack.item.icon, itemStack.item.Name, itemStack.quantity);
        }

        private void SpawnOrderElement(Sprite itemSprite, string itemName, int itemQuantity)
        {
            GameObject selectedSlot = null;
            foreach (var slot in orderPartsSlots)
            {
                if (!slot.activeSelf)
                {
                    selectedSlot = slot;
                    break;
                }
            }
            if (selectedSlot == null)
                return;
            selectedSlot.SetActive(true);
            var newElement = Instantiate(orderPartPrefab, selectedSlot.transform);
            newElement.GetComponent<UiOrderPartController>().Init(itemSprite, itemName, itemQuantity);
        }

        internal void Clear()
        {
            foreach (var partSlot in orderPartsSlots)
            {
                if (partSlot.transform.childCount > 0)
                    for (var i = 0; i < partSlot.transform.childCount; i++)
                        Destroy(partSlot.transform.GetChild(i).gameObject);
                partSlot.SetActive(false);
            }
        }
    }
}
