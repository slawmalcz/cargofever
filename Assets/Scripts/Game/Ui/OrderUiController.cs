﻿using Game.Data;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    public class OrderUiController : MonoBehaviour
    {
        public GameObject OrderUi = null;
        public OrderUiTimerController orderTimerController = null;
        public UiOrderPartsController orderPartController = null;
        public Text orderGateText = null;

        public bool IsSlotActive {
            get => OrderUi.activeInHierarchy;
            set {
                if (!value)
                    ClearOrderSlot();
                OrderUi.SetActive(value);
            }
        }

        public Contract contract = null;

        public void Init(Contract contract)
        {
            this.contract = contract;
            orderPartController.Init(contract.name, contract.Manifest);
            orderGateText.text = contract.assignedGate.ToString();

            IsSlotActive = true;
        }

        public void Update()
        {
            if (IsSlotActive == true)
                orderTimerController.UpdateSlider(contract.TimePastPercentage, contract.timeToCompleat - contract.timeToCompleat * contract.TimePastPercentage);
        }

        private void ClearOrderSlot()
        {
            orderPartController.Clear();
            contract = null;
        }
    }
}
