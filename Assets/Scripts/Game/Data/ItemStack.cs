﻿using Game.Entities;

namespace Game.Data
{
    public class ItemStack
    {
        public int quantity = 0;
        public readonly Item item;
        public int Value => quantity * item.value;
        public float TimeToCompleat => quantity * item.timeForPrepering;

        public ItemStack(Item item, int quantity = 0)
        {
            this.item = item;
            this.quantity = quantity;
        }

        public static ItemStack operator -(ItemStack itemStack, int quantity)
        {
            itemStack.quantity -= quantity;
            return itemStack;
        }
        public static ItemStack operator +(ItemStack itemStack, int quantity)
        {
            itemStack.quantity += quantity;
            return itemStack;
        }
    }
}
