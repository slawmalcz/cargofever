﻿using CsvReader;
using Game.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game.Data
{
    [Serializable]
    [CreateAssetMenu(fileName = "ContractModel", menuName = "ScriptableObjects/ContractsModel", order = 2)]
    class ContractsModels : ScriptableObject
    {
        private const string DECODING_KEY = "EXE";
        private const string ITEM_NAME = "Name";
        private const string ITEM_QUANTITY = "Quantity";

        private const string EXEC_KEY_NEW = "NEW";
        private const string EXEC_KEY_INPUT = "IN";

        public string sceneName = "";
        public TextAsset contractsCsv = null;
        public List<ContractModel> Contracts {
            get {
                var ret = new List<ContractModel>();

                var dataGrid = CsvFileReader.ReadAll(new MemoryStream(contractsCsv.bytes), Encoding.GetEncoding("UTF-8"));
                var indexOfDecodingKey = dataGrid[0].IndexOf(DECODING_KEY);
                var indexOfName = dataGrid[0].IndexOf(ITEM_NAME);
                var indexOfQuantity = dataGrid[0].IndexOf(ITEM_QUANTITY);
                ContractModel contractModel = null;
                foreach (var row in dataGrid.Skip(1))
                {
                    if (row[indexOfDecodingKey] == EXEC_KEY_NEW)
                    {
                        contractModel = new ContractModel(row[indexOfName]);
                        ret.Add(contractModel);
                    }
                    else if (row[indexOfDecodingKey] == EXEC_KEY_INPUT)
                    {
                        contractModel.AddOrder(int.Parse(row[indexOfQuantity]), ResourcesController.Instance.GetItem(row[indexOfName]));
                    }
                }
                return ret;
            }
        }
    }
}
