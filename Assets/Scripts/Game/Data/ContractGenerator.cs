﻿using Game.Entities;
using System.Collections.Generic;

namespace Game.Data
{
    class ContractGenerator
    {
        private readonly List<ContractModel> aviableContracts = null;

        public ContractGenerator(string levelName) => aviableContracts = ResourcesController.Instance.GetContracts(levelName).Contracts;

        public Contract GenerateContract()
         {
            var contractNumber = UnityEngine.Random.Range(0, aviableContracts.Count);
            var selectedContract = aviableContracts[contractNumber].DeapClone();

            return new Contract(selectedContract.name, selectedContract.manifest.UnderEquipedValue, selectedContract.manifest.TimeToCompelat, selectedContract.manifest);
        }
    }
}
