﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
    public class Item : ScriptableObject
    {
        [SerializeField]
        public string Id = "Itm_";
        public string Name = "";
        public Sprite icon = null;
        public int value = 0;
        public float timeForPrepering = 0;

        public GameObject prefab = null;

        public bool Equals(Item other) => Id == other.Id;
        public static bool operator ==(Item a, Item b) => a.Equals(b);
        public static bool operator !=(Item a, Item b) => !a.Equals(b);
        public override bool Equals(object other) => other is Item && Equals(((Item)other));

        public override int GetHashCode()
        {
            var hashCode = 1154482552;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Id);
            hashCode = hashCode * -1521134295 + EqualityComparer<Sprite>.Default.GetHashCode(icon);
            hashCode = hashCode * -1521134295 + value.GetHashCode();
            hashCode = hashCode * -1521134295 + timeForPrepering.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<GameObject>.Default.GetHashCode(prefab);
            return hashCode;
        }
    }
}

