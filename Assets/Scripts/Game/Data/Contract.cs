﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Data
{
    public class Contract
    {
        public const float VERY_FAST_DELIVERY = 0.25f;
        public const float NORMAL_DELIVERY = 0.6f;
        public const float VERY_LATE_DELIVERY = 0.8f;
        public const float DELAYED_DELIVERY = 1f;

        private const float VERY_FAST_BONUS = 0.1f;
        private const float OFF_TIME_REDUCTION = 0.5f;

        public readonly float timeToCompleat = 0;
        private readonly float startTime = 0;
        private float currentTime = 0;

        public event Action<Contract> OnTerminateEvent = delegate { };

        public float TimePastPercentage => (currentTime - startTime) / timeToCompleat;

        public Manifest Manifest { get; private set; } = null;

        private readonly int basePoints = 0;
        private int pointsBoon = 0;
        public int Points {
            get {
                pointsBoon += Manifest.UnderEquipedValue + Manifest.OverEquipedValue;
                if (TimePastPercentage <= VERY_FAST_DELIVERY)
                    return basePoints + (int)Math.Round(VERY_FAST_BONUS * basePoints) - pointsBoon;
                else if (TimePastPercentage <= NORMAL_DELIVERY)
                    return (int)Math.Round(basePoints * (1 - TimePastPercentage)) - pointsBoon;
                else if (TimePastPercentage <= DELAYED_DELIVERY)
                    return (int)Math.Round(basePoints * OFF_TIME_REDUCTION) - pointsBoon;
                else
                    return Manifest.UnderEquipedValue + Manifest.OverEquipedValue;
            }
        }

        public int assignedGate = 0;
        public readonly string name = null;

        public Contract(string name, int basePoints, float timeToCompleat, Manifest manifest)
        {
            this.name = name;
            Manifest = manifest;
            this.basePoints = basePoints;
            this.timeToCompleat = timeToCompleat;
            startTime = Time.fixedTime;
            currentTime = startTime;
        }

        public void TickContract() => currentTime = Time.fixedTime;

        public void EnbarkCargo(List<Item> paletItems)
        {
            var loadingManifest = CreateLoadingMainfest(paletItems);
            foreach (var itemStack in loadingManifest)
                Manifest -= itemStack;
        }

        private List<ItemStack> CreateLoadingMainfest(List<Item> paletItem)
        {
            var ret = new Manifest();
            foreach (var item in paletItem)
                ret += new ItemStack(item, 1);
            return ret.ItemStacks;
        }

        public void TerminateContract()
        {
            OnTerminateEvent.Invoke(this);
            OnTerminateEvent = delegate { };
        }
    }
}