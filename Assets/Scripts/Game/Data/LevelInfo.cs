﻿using UnityEngine;

namespace Game.Data
{
    [CreateAssetMenu(fileName = "LevelInfo", menuName = "ScriptableObjects/Level_Info", order = 2)]
    public class LevelInfo : ScriptableObject
    {
        public string levelName = "";

        public string levelScene = "";

        public float levelTime = 0;

        public int requiredPoints = 0;

        public int[] pointsToLevel;
    }
}
