﻿using System.Collections.Generic;
using System.Linq;

namespace Game.Data
{
    public class Manifest
    {
        public List<ItemStack> ItemStacks { get; private set; } = null;

        internal ItemStack this[Item key] {
            get => ItemStacks.FirstOrDefault(x => x.item == key);
            private set {
                var itemStackCandidate = ItemStacks.FirstOrDefault(x => x.item == key);
                if (itemStackCandidate == null)
                    ItemStacks.Add(value);
                else
                    itemStackCandidate = value;
                TrimManifest();
            }
        }

        public int Value => OverEquipedValue + UnderEquipedValue;
        public int OverEquipedValue => -ItemStacks.Where(x => x.quantity < 0).Sum(x => x.Value);
        public int UnderEquipedValue => ItemStacks.Where(x => x.quantity > 0).Sum(x => x.Value);

        public float TimeToCompelat => ItemStacks.Where(x => x.quantity > 0).Sum(x => x.TimeToCompleat);

        public Manifest() => ItemStacks = new List<ItemStack>();

        public Manifest(List<ItemStack> itemStacks) => ItemStacks = itemStacks;

        private void TrimManifest() => ItemStacks.RemoveAll(x => x.quantity == 0);

        private bool Contains(Item key)
        {
            var itemStackCandidate = ItemStacks.FirstOrDefault(x => x.item == key);
            return itemStackCandidate != null;
        }

        public static Manifest operator -(Manifest manifest, ItemStack itemStack)
        {
            if (manifest.Contains(itemStack.item))
                manifest[itemStack.item] -= itemStack.quantity;
            else
                manifest[itemStack.item] = new ItemStack(itemStack.item, -itemStack.quantity);
            return manifest;
        }

        public static Manifest operator +(Manifest manifest, ItemStack itemStack)
        {
            if (manifest.Contains(itemStack.item))
                manifest[itemStack.item] += itemStack.quantity;
            else
                manifest[itemStack.item] = itemStack;
            return manifest;
        }
    }
}
