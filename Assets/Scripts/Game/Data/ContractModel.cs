﻿namespace Game.Data
{
    class ContractModel
    {
        public readonly string name = null;
        public Manifest manifest = null;

        public ContractModel(string name)
        {
            this.name = name;
            manifest = new Manifest();
        }

        public void AddOrder(int quantity, Item item) => manifest += new ItemStack(item, quantity);

        public ContractModel DeapClone()
        {
            var ret = new ContractModel(name);
            foreach(var order in manifest.ItemStacks)
            {
                ret.AddOrder(order.quantity, order.item);
            }
            return ret;
        }
    }
}
