﻿using Game.Utilities;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Transport
{
    public abstract class AbstractTruckController : MonoBehaviour
    {
        [Header("Paths")]
        public AutomaticDriveSpineLine enterPath = null;
        public AutomaticDriveSpineLine exitPath = null;

        public AutomaticDriveController driveController;
        public bool IsInTransition => driveController.IsDuringTransfer;

        public UnityEvent OnDeparture = null;
        public UnityEvent OnArrival = null;

        public virtual void Request()
        {
            enterPath.StartTravel(driveController);
            OnDeparture.Invoke();
            driveController.OnDestinationReached += () => OnArrival.Invoke();
        }

        public virtual void Send()
        {
            exitPath.StartTravel(driveController);
            OnDeparture.Invoke();
            driveController.OnDestinationReached += () => { Destroy(gameObject); };
        }
    }
}
