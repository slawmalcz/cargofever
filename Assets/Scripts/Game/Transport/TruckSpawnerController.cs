﻿using System.Linq;
using UnityEngine;

namespace Game.Transport
{
    public class TruckSpawnerController : MonoBehaviour
    {
        public AbstractAreaController[] sendingAreas = null;

        public GameObject truckPrefab = null;

        protected AbstractTruckController DefaultSpawn(int destinationAreaNumber)
        {
            var truck = Instantiate(truckPrefab);
            var truckController = truck.GetComponent<AbstractTruckController>();
            var destinationSendingArea = sendingAreas.Where(x => x.areaNumber == destinationAreaNumber).First();
            truck.transform.parent = destinationSendingArea.gameObject.transform;
            truck.transform.position = gameObject.transform.position;
            truck.transform.localEulerAngles = Vector3.zero;
            destinationSendingArea.AssginTruck(truckController);
            truckController.Request();
            return truckController;
        }

        public virtual AbstractTruckController Spawn(int destinationAreaNumber) => DefaultSpawn(destinationAreaNumber);
    }
}
