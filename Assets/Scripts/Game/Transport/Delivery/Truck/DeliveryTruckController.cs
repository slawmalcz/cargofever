﻿using Game.Data;
using Game.Entities;
using UnityEngine;

namespace Game.Transport.Delivery
{
    public class DeliveryTruckController : AbstractTruckController
    {
        public GameObject palletPrefab = null;

        public Transform[] palletsSlots = null;

        public void GenerateCargo(Item[] allowedItems)
        {
            foreach (var slot in palletsSlots)
            {
                var pallet = Instantiate(palletPrefab, slot);
                var palletController = pallet.GetComponent<PalletController>();
                for (var i = 0; i < palletController.itemSlotsTransform.Length; i++)
                {
                    var randItem = allowedItems[Random.Range(0, allowedItems.Length)];
                    palletController.SpawnItem(randItem);
                }
                pallet.GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                pallet.GetComponent<Rigidbody>().isKinematic = true;
            }
        }

        public override void Request()
        {
            enterPath.StartTravel(driveController);
            driveController.OnDestinationReached +=
                () => {
                    OnArrival.Invoke();
                    foreach (var palletSlot in palletsSlots)
                    {
                        if (palletSlot.childCount < 1)
                            continue;
                        var palletRigidbody = palletSlot.GetChild(0).gameObject.GetComponent<Rigidbody>();
                        if (palletRigidbody != null)
                        {
                            palletRigidbody.isKinematic = false;
                            palletRigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
                        }
                    }
                };
        }

        public override void Send()
        {
            OnDeparture.Invoke();
            foreach (var palletSlot in palletsSlots)
            {
                if (palletSlot.childCount < 1)
                    continue;
                var palletRigidbody = palletSlot.GetChild(0).gameObject.GetComponent<Rigidbody>();
                if (palletRigidbody != null)
                {
                    palletRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                    palletRigidbody.isKinematic = true;
                }
            }
            exitPath.StartTravel(driveController);
            driveController.OnDestinationReached += () => { Destroy(gameObject); };
        }
    }
}

