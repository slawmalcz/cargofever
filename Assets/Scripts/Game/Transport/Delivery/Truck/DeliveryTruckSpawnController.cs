﻿using System.Linq;
using UnityEngine;

namespace Game.Transport.Delivery
{
    public class DeliveryTruckSpawnController : TruckSpawnerController
    {
        public AllowedItemGroup[] allowedGroups = null;

        public void Start()
        {
            if (allowedGroups.Length < 1)
                Debug.LogError($"DeliveryTruckSpawnController {gameObject.name} have no established allowedGroups");
        }


        public override AbstractTruckController Spawn(int deliveryAreaNumber)
        {
            var allowedItemGroup = allowedGroups.ToList().FirstOrDefault(x => x.area.areaNumber == deliveryAreaNumber);
            if(allowedItemGroup == null)
            {
                Debug.LogError($"DeliveryTruckSpawnController {gameObject.name} have no established allowedGroups for deliveryArea {deliveryAreaNumber} \n" +
                               $"Assigned default group 'allowedGroups[0]'");
                allowedItemGroup = allowedGroups.ToList().First();
            }
            var truckController = DefaultSpawn(deliveryAreaNumber);
            ((DeliveryTruckController)truckController).GenerateCargo(allowedItemGroup.itemsList);
            return truckController;
        }
    }
}

