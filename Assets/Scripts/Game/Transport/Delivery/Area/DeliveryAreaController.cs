﻿using System.Linq;
using UnityEngine;

namespace Game.Transport.Delivery
{
    public class DeliveryAreaController : AbstractAreaController
    {
        public void Start()
        {
            if (FindObjectsOfType<DeliveryAreaController>().ToList().Any(x => x.areaNumber == areaNumber && x != this))
                Debug.LogError($"Multiple of {this.GetType().Name} have this same areaNumber {gameObject.name}" +
                    $"i have {areaNumber}");
        }

        public override void RequestTruck() => spawnController.Spawn(areaNumber);
        public override void SendTruck() => assignedTruck.Send();
    }
}

