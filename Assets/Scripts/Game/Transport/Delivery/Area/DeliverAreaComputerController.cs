﻿using Game.Entities;

namespace Game.Transport.Delivery
{
    public class DeliverAreaComputerController : AbstractAreaComputerController
    {
        public override bool Interact(PlayerController player)
        {
            if (assignedArea.assignedTruck?.IsInTransition ?? false)
                return false;
            ForcePosition(player);
            player.ApplyUsePc();
            if (assignedArea.assignedTruck == null)
                assignedArea.RequestTruck();
            else
                assignedArea.SendTruck();
            return true;
        }
    }
}

