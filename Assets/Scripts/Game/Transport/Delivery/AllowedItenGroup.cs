﻿using Game.Data;
using System;

namespace Game.Transport.Delivery
{
    [Serializable]
    public class AllowedItemGroup
    {
        public DeliveryAreaController area;
        public Item[] itemsList;
    }
}
