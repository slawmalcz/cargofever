﻿using Game.Entities;
using Game.Utilities;
using UnityEngine;

namespace Game.Transport
{
    public abstract class AbstractAreaComputerController : MonoBehaviour, ICharacterInteractive
    {
        public CharacterInteractiveController interactiveController;
        public AbstractAreaController assignedArea = null;
        public Transform playerUsePosition = null;

        public GameObject AttachedGameObject => gameObject;

        public abstract bool Interact(PlayerController player);

        protected void ForcePosition(PlayerController player)
        {
            player.transform.position = playerUsePosition.transform.position;
            player.transform.rotation = playerUsePosition.transform.rotation;
        }
    }
}
