﻿using Game.Utilities;
using UnityEngine;

namespace Game.Transport
{
    public abstract class AbstractAreaController : MonoBehaviour
    {
        [Header("AreaOptions")]
        public int areaNumber = 0;
        public AbstractTruckController assignedTruck = null;
        public TruckSpawnerController spawnController = null;
        [Header("Paths")]
        public AutomaticDriveSpineLine exitPath = null;
        public AutomaticDriveSpineLine enterPath = null;

        public abstract void SendTruck();
        public abstract void RequestTruck();

        public void AssginTruck(AbstractTruckController deliveryTruck)
        {
            assignedTruck = deliveryTruck;
            deliveryTruck.enterPath = enterPath;
            deliveryTruck.exitPath = exitPath;
        }
    }
}
