﻿using Game.Entities;

namespace Game.Transport.Sending
{
    class SendingAreaComputerController : AbstractAreaComputerController
    {
        public override bool Interact(PlayerController player)
        {
            if (assignedArea.assignedTruck?.IsInTransition ?? true)
                return false;
            ForcePosition(player);
            player.ApplyUsePc();
            assignedArea.SendTruck();
            return true;
        }
    }
}
