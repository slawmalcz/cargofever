﻿using Game.Data;
using Game.Entities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Transport.Sending
{
    public class SendingAreaController : AbstractAreaController
    {
        public Contract truckContract = null;

        public void Start()
        {
            if (FindObjectsOfType<SendingAreaController>().ToList().Any(x => x.areaNumber == areaNumber && x != this))
                Debug.LogError($"Multiple of {GetType().Name} have this same areaNumber");
        }

        public void AssignContract(Contract contract)
        {
            truckContract = contract;
            contract.assignedGate = areaNumber;
        }

        public override void SendTruck()
        {
            var immutableData = (IReadOnlyList<GameObject>)((SendingTruckController)assignedTruck).LoadedCargoList;
            for (var i = 0; i < immutableData.Count; i++)
            {
                var cargo = immutableData[i];
                truckContract.EnbarkCargo(cargo.GetComponent<PalletController>().ItemList);
                if (((SendingTruckController)assignedTruck).canBePalletFedded)
                    ((SendingTruckController)assignedTruck).AttachCargoToSend(cargo);
            }

            truckContract.TerminateContract();
            assignedTruck.Send();
        }

        public override void RequestTruck() => spawnController.Spawn(areaNumber);
    }
}
