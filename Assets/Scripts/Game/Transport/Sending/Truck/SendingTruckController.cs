﻿using Game.Entities;
using Game.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Transport.Sending
{
    class SendingTruckController : AbstractTruckController, ITagDetector
    {
        private TriggerPasserHandler _triggerHandler = null;
        public TriggerPasserHandler TriggerHandler {
            get {
                if (_triggerHandler == null)
                    _triggerHandler = GetComponent<TriggerPasserHandler>();
                return _triggerHandler;
            }
        }

        public Transform[] cargoPositions = null;
        public bool canBePalletFedded = false;
        public List<GameObject> LoadedCargoList {
            get {
                if (canBePalletFedded)
                    return loadedCargoList;
                var ret = new List<GameObject>();
                foreach (var slot in cargoPositions)
                {
                    if (slot.transform.childCount > 0)
                    {
                        ret.Add(slot.GetComponentInChildren<PalletController>().gameObject);
                    }
                }
                return ret;

            }
        }
        private List<GameObject> loadedCargoList = null;

        public void TriggerHandlerEvent(GameObject other, GameObject detector, bool isEnter)
        {
            if (loadedCargoList == null)
                loadedCargoList = new List<GameObject>();
            if (isEnter)
                loadedCargoList.Add(other);
            else
                if (loadedCargoList.Contains(other))
                loadedCargoList.Remove(other);
        }

        internal void AttachCargoToSend(GameObject cargo)
        {
            foreach (var slot in cargoPositions)
            {
                if (slot.transform.childCount == 0)
                {
                    var rebuildedCargo = Instantiate(cargo, slot);
                    rebuildedCargo.transform.rotation = Quaternion.identity;
                    rebuildedCargo.transform.localPosition = Vector3.zero;
                    break;
                }
            }
            loadedCargoList.Remove(gameObject);
        }

        public override void Send()
        {
            OnDeparture.Invoke();
            foreach (var palletSlot in cargoPositions)
            {
                if (palletSlot.childCount < 1)
                    continue;
                var palletRigidbody = palletSlot.GetChild(0).gameObject.GetComponent<Rigidbody>();
                if (palletRigidbody != null)
                {
                    palletRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                    palletRigidbody.isKinematic = true;
                }
            }
            exitPath.StartTravel(driveController);
            driveController.OnDestinationReached += () => { Destroy(gameObject); };
        }
    }
}
