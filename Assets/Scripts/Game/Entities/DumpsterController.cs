﻿using UnityEngine;

namespace Game.Entities
{
    public class DumpsterController : MonoBehaviour
    {
        public string detectedTag = "Cargo";

        public void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(detectedTag))
                Destroy(other.gameObject);
        }
    }
}
