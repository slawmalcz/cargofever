﻿using System;
using System.Linq;
using Game.Data;
using Game.Transport.Sending;
using Game.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Entities
{
    public class ComputerController : MonoBehaviour, ICharacterInteractive
    {
        [Header("Required components")]
        public CharacterInteractiveController interactiveController = null;
        public WorldController worldController = null;
        public SendingAreaController[] sendingAreaControllers = null;
        public Transform playerUsePosition = null;
        private ContractGenerator contractGenerator = null;

        public GameObject AttachedGameObject => gameObject;

        public void Start()
        {
            contractGenerator = new ContractGenerator(SceneManager.GetActiveScene().name);
        }

        public bool Interact(PlayerController player)
        {
            if (sendingAreaControllers.All(x => x.assignedTruck != null))
                return false;
            ForcePosition(player);
            var selectedArea = sendingAreaControllers.First(x => x.assignedTruck == null);
            var newContract = contractGenerator.GenerateContract();
            newContract.OnTerminateEvent += AddPoints;
            worldController.ordersUiController.AddContract(newContract);
            selectedArea.AssignContract(newContract);
            selectedArea.RequestTruck();
            player.ApplyUsePc();
            return true;

        }

        private void ForcePosition(PlayerController player)
        {
            player.transform.position = playerUsePosition.transform.position;
            player.transform.rotation = playerUsePosition.transform.rotation;
        }

        private void AddPoints(Contract contract) => worldController.points += contract.Points;
    }
}

