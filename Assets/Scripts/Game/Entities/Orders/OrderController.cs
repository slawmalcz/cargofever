﻿using Game.Data;
using Game.Transport.Sending;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Entities
{
    public class OrderController : MonoBehaviour
    {
        [Header("Required components")]
        public WorldController worldController = null;
        public SendingAreaController[] sendingAreaControllers = null;
        private ContractGenerator contractGenerator = null;
        public OcuppationTrigger spawnerOccupationTrigger = null;

        public float awaitTime = 10;
        public float maxAdditionalTime = 3;
        private int currentRunnigGeneration = 0;

        public GameObject AttachedGameObject => gameObject;

        public void Start()
        {
            contractGenerator = new ContractGenerator(SceneManager.GetActiveScene().name);
        }

        public void Update()
        {
            if (sendingAreaControllers.All(x => x.assignedTruck != null))
                return;
            var contractsToGenerate = sendingAreaControllers.Count(x => x.assignedTruck == null) - currentRunnigGeneration;
            for (var i = 0; i < contractsToGenerate; i++)
            {
                var timeToStart = awaitTime + Random.value * maxAdditionalTime;
                currentRunnigGeneration++;
                StartCoroutine(Generate(timeToStart));
            }

        }

        public IEnumerator Generate(float time)
        {
            yield return new WaitForSeconds(time);

            if (spawnerOccupationTrigger.IsFree)
            {
                var selectedArea = sendingAreaControllers.First(x => x.assignedTruck == null);
                var newContract = contractGenerator.GenerateContract();
                newContract.OnTerminateEvent += AddPoints;
                selectedArea.AssignContract(newContract);
                selectedArea.RequestTruck();
                worldController.ordersUiController.AddContract(newContract);
            }
            currentRunnigGeneration--;
        }

        private void AddPoints(Contract contract) => worldController.points += contract.Points;
    }
}

