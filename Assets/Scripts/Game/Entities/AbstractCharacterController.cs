﻿using System;
using UnityEngine;

namespace Game.Entities
{
    [RequireComponent(typeof(Rigidbody))]
    public abstract class AbstractCharacterController : MonoBehaviour
    {
        public Rigidbody playerRigidbody = null;
        public bool HaveControll {
            get => _haveControll;
            set {
                OnHaveControllChange.Invoke(_haveControll, value);
                _haveControll = value;
            }
        }
        [SerializeField]
        private bool _haveControll = false;
        public event Action<bool, bool> OnHaveControllChange = delegate { };

        protected virtual void Initialize() { }

        private void Start()
        {
            playerRigidbody = GetComponent<Rigidbody>();
            Initialize();
        }
    }
}
