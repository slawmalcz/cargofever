﻿using Game.Data;
using Game.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Entities
{
    class ProductionController : MonoBehaviour, ICharacterInteractive
    {
        public Transform[] substractsSlots = null;
        public Transform[] productsSlots = null;
        public CreationRecepy[] allowedReceptures = null;

        public List<Item> Substracts => substractsSlots.
                                        Where(x => !IsFree(x))
                                       .Select(x => x.GetChild(0).gameObject.GetComponent<ItemController>().item)
                                       .ToList();

        public GameObject AttachedGameObject => gameObject;

        private bool IsFree(Transform itemslot) => itemslot.transform.childCount == 0;

        public bool Interact(PlayerController player)
        {
            if (player.IsCarryObject)
            {
                //Puting down
                var placeSlotsCollections = substractsSlots.Concat(productsSlots);
                var selectedSpot = FindColsestSpot(player.transform.position, placeSlotsCollections.ToArray());
                if (selectedSpot != null)
                    Instantiate(player.PutDown().prefab, selectedSpot);
            }
            else
            {
                // Picking up
                var placeSlotsCollections = substractsSlots.Concat(productsSlots);
                var selectedSpot = FindColsestSpot(player.transform.position, placeSlotsCollections.ToArray(), false);
                if (selectedSpot != null)
                {
                    var selectedObject = selectedSpot.GetChild(0).gameObject;
                    player.PickUp(selectedObject.GetComponent<ItemController>().item);
                    Destroy(selectedObject);
                }
            }
            return true;
        }

        /// <summary>
        /// External activation by <see cref="SwitchController"/>
        /// </summary>
        public void Activate()
        {
            var selected = SelectRecepy();
            if (selected != null)
                SpawnProducts(selected);
        }

        private CreationRecepy SelectRecepy()
        {
            foreach (var recepy in allowedReceptures)
                if (recepy.IsFullfilled(Substracts))
                    return recepy;
            return null;
        }

        private void SpawnProducts(CreationRecepy selectedRecepy)
        {
            //List of leftovers
            var leftOvers = Substracts;
            leftOvers.RemoveAll(x => selectedRecepy.Substracts.Contains(x) || x is null);
            //Deleting subtracts
            substractsSlots.ToList().ForEach(x => DestroyAllChildrens(x));
            //restore leftovers
            leftOvers.ForEach(x => Instantiate(x, substractsSlots.First(y => IsFree(y))));
            //Generate products
            selectedRecepy.Products.ForEach(x => Instantiate(x.prefab, productsSlots.First(y => IsFree(y))));
        }


        public Transform FindColsestSpot(Vector3 platerPosition, Transform[] positions, bool isFree = true)
        {
            Transform closesSlot = null;
            for (var i = 0; i < positions.Length; i++)
            {
                if (IsFree(positions[i]) == isFree)
                {
                    closesSlot = positions[i];
                    break;
                }
            }

            if (closesSlot == null)
                return null;

            var minimalDistance = Vector3.Distance(closesSlot.position, platerPosition);
            for (var i = 1; i < positions.Length; i++)
            {
                var distance = Vector3.Distance(positions[i].transform.position, platerPosition);
                if (distance < minimalDistance && IsFree(positions[i]) == isFree)
                {
                    minimalDistance = distance;
                    closesSlot = positions[i];
                }
            }
            return closesSlot;
        }

        public void DestroyAllChildrens(Transform transform)
        {
            for (var i = 0; i < transform.childCount; i++)
                Destroy(transform.GetChild(i).gameObject);
        }
    }
}
