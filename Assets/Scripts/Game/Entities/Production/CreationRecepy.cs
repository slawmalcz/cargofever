﻿using Game.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Entities
{
    [CreateAssetMenu(fileName = "CreationRecepy", menuName = "ScriptableObjects/CreationRecepy", order = 4)]
    internal class CreationRecepy : ScriptableObject
    {
        //Initializers
        [SerializeField]
        private Item[] sunstracts = null;
        [SerializeField]
        private Item[] products = null;
        
        //Data holders
        private List<Item> _substracts = null;
        public List<Item> Substracts {
            get {
                if (_substracts == null || _substracts.Count != sunstracts.Length)
                    _substracts = new List<Item>(sunstracts);
                return _substracts;
            }
        }

        private List<Item> _products = null;
        public List<Item> Products {
            get {
                if (_products == null || _products.Count != products.Length)
                    _products = new List<Item>(products);
                return _products;
            }
        }

        public bool IsFullfilled(List<Item> substracts) => Substracts.All(x => substracts.Contains(x));
    }
}