﻿using Game.Data;
using Game.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Entities
{
    class PalletController : MonoBehaviour, ICharacterInteractive, ICharacterHoldInteractive, IForkLiftInteractive
    {
        public Transform[] itemSlotsTransform = null;

        public List<Item> ItemList {
            get {
                var ret = new List<Item>();
                foreach (var slot in itemSlotsTransform)
                {
                    if (slot.childCount > 0 && slot.GetChild(0).gameObject.GetComponent<ItemController>() != null)
                        ret.Add(slot.GetChild(0).gameObject.GetComponent<ItemController>().item);
                }
                return ret;
            }
        }

        public GameObject AttachedGameObject => (this == null) ? null : gameObject;

        private bool IsFree(Transform itemslot) => itemslot.transform.childCount == 0;

        public void SpawnItem(Item item)
        {
            var closesSlot = FindColsestSpot(Vector3.zero);
            if (closesSlot == null)
                return;
            Instantiate(item.prefab, closesSlot);
            ItemList.Add(item);
        }

        public Transform FindColsestSpot(Vector3 platerPosition, bool isFree = true)
        {
            Transform closesSlot = null;
            for (var i = 0; i < itemSlotsTransform.Length; i++)
            {
                if (IsFree(itemSlotsTransform[i]) == isFree)
                {
                    closesSlot = itemSlotsTransform[i];
                    break;
                }
            }

            if (closesSlot == null)
                return null;

            var minimalDistance = Vector3.Distance(closesSlot.position, platerPosition);
            for (var i = 1; i < itemSlotsTransform.Length; i++)
            {
                var distance = Vector3.Distance(itemSlotsTransform[i].transform.position, platerPosition);
                if (distance < minimalDistance && IsFree(itemSlotsTransform[i]) == isFree)
                {
                    minimalDistance = distance;
                    closesSlot = itemSlotsTransform[i];
                }
            }
            return closesSlot;
        }

        // ---- Character Dependent Function ----

        public bool Interact(PlayerController player)
        {
            if (this == null)
                return false;
            if (player.IsCarryObject)
                PlaceItem(player);
            else
                PickUpItem(player);
            return true;
        }

        public bool HoldInteractBegine(PlayerController player)
        {
            if (this == null)
                return false;
            player.playerDriver.IsRotationLocked = true;
            transform.parent = player.transform;
            GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
            GetComponent<Rigidbody>().isKinematic = true;
            return true;
        }

        public bool HoldInteractEnd(PlayerController player)
        {
            if (this == null)
                return false;
            player.playerDriver.IsRotationLocked = false;
            transform.parent = null;
            GetComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.Continuous;
            GetComponent<Rigidbody>().isKinematic = false;
            return true;
        }

        private void PlaceItem(PlayerController player)
        {
            var closesSlot = FindColsestSpot(player.transform.position);
            if (closesSlot == null)
                return;
            Instantiate(player.PutDown().prefab, closesSlot); ;
        }

        private void PickUpItem(PlayerController player)
        {
            var closesSlot = FindColsestSpot(player.transform.position, false);
            if (closesSlot != null)
            {
                var itemcontroller = closesSlot.GetChild(0).gameObject.GetComponent<ItemController>();
                player.PickUp(itemcontroller.item);
                Destroy(closesSlot.GetChild(0).gameObject);
            }

        }

        // ---- Character Dependent Function ----

        public void Interact(ForkLiftController forkLift)
        {
            if (this == null || !forkLift.IsPalletSlotFree)
                return;
            if (forkLift.IsPalletSlotFree)
            {
                forkLift.carridePallet = gameObject;
                forkLift.palletSlot.GetComponent<FixedJoint>().connectedBody = gameObject.GetComponent<Rigidbody>();
            }
        }

        private void OnDestroy()
        {
            if (WorldController.Instance.player.CurrentHoldedObject == (ICharacterHoldInteractive)this)
                WorldController.Instance.player.playerDriver.IsRotationLocked = false;
        }
    }
}
