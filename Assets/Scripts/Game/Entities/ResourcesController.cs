﻿using Game.Data;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Entities
{
    class ResourcesController
    {
        private const string ITEMS_PATH = "Items";
        private const string COMTRACT_MODELS_PATH = "Contracts/Models";
        private const string LEVEL_INFOS_PATH = "Level_Info";

        //SingletonImplementation
        public static ResourcesController Instance {
            get {
                if (_instance == null)
                    _instance = new ResourcesController();
                return _instance;
            }
        }

        private static ResourcesController _instance = null;
        // Body
        private readonly Dictionary<string, Item> itemsScriptableObjects = null;
        private readonly Dictionary<string, ContractsModels> levelRequestsScriptableObject = null;
        private readonly Dictionary<string, LevelInfo> levelInfoScriptableObjects = null;

        private ResourcesController()
        {
            itemsScriptableObjects = new Dictionary<string, Item>();
            itemsScriptableObjects = Resources.LoadAll<Item>(ITEMS_PATH).ToDictionary(x => x.Id, x => x);
            levelRequestsScriptableObject = new Dictionary<string, ContractsModels>();
            levelRequestsScriptableObject = Resources.LoadAll<ContractsModels>(COMTRACT_MODELS_PATH).ToDictionary(x => x.sceneName, x => x);
            levelInfoScriptableObjects = new Dictionary<string, LevelInfo>();
            levelInfoScriptableObjects = Resources.LoadAll<LevelInfo>(LEVEL_INFOS_PATH).ToDictionary(x => x.levelScene, x => x);
        }

        public Item GetItem(string name)
        {
            itemsScriptableObjects.TryGetValue(name, out var value);
            if (value is null)
                Debug.LogWarning($"Resource controller try to access not existing item {name}");
            return value;
        }

        public ContractsModels GetContracts(string sceneName)
        {
            levelRequestsScriptableObject.TryGetValue(sceneName, out var value);
            if (value == null)
                Debug.LogWarning($"Resource controller try to access not existing contract {sceneName}");
            return value;
        }

        public LevelInfo GetLevelInfo(string sceneName)
        {
            levelInfoScriptableObjects.TryGetValue(sceneName, out var value);
            if (value == null)
                Debug.LogWarning($"Resource controller try to access not existing contract {sceneName}");
            return value;
        }
    }
}
