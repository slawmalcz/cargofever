﻿using UnityEngine;

namespace Game.Entities
{
    class PalletDispenser : MonoBehaviour
    {
        public GameObject palletGameObjectPrefab = null;
        public Transform palletSpawnArea = null;

        public void SpawnPallet()
        {
            var newPalet = Instantiate(palletGameObjectPrefab,palletSpawnArea);
            newPalet.transform.parent = null;
        }
    }
}
