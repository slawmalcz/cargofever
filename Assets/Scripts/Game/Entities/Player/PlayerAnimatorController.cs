﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Entities
{

    public struct AnimatorValues
    {
        internal float angularVelocity;
        internal float velocity;

        public AnimatorValues(float angularVelocity, float velocity) : this()
        {
            this.angularVelocity = angularVelocity;
            this.velocity = velocity;
        }
    }

    public class Trigger { }

    public class AnimatorProperty<T>
    {
        private static Dictionary<Type, Action<Animator, string, object>> actionPasser = new Dictionary<Type, Action<Animator, string, object>>
        {
            {typeof(int),(Animator animator,string tag,object value) => { animator.SetInteger(tag,(int) value); } },
            {typeof(float),(Animator animator,string tag,object value) => { animator.SetFloat(tag,(float) value); } },
            {typeof(bool),(Animator animator,string tag,object value) => { animator.SetBool(tag,(bool) value); } },
            {typeof(Trigger),(Animator animator,string tag,object value) => { animator.SetTrigger(tag); } }
        };

        private readonly Animator animator;
        private readonly string tag;
        private readonly Action<T> assignedSetAction;
        private T _value = default;

        public void Set() => Set(default);
        public void Set(T value)
        {
            _value = value;
            assignedSetAction.Invoke(_value);
        }
        public T Get() => _value;

        public AnimatorProperty(Animator animator, string tag)
        {
            this.animator = animator;
            this.tag = tag;
            if (!actionPasser.ContainsKey(typeof(T)))
                throw new Exception($"Animator can't use value of type {typeof(T).Name}");
            assignedSetAction = SimplyfySettingAction(actionPasser[typeof(T)]);
        }

        private Action<T> SimplyfySettingAction(Action<Animator, string, object> action) =>
            (T value) => { action.Invoke(animator, tag, (object)value); };
    }

    public class AnimatorProperty
    {

        private readonly Animator animator;
        private readonly string tag;

        public void Set()
        {
            animator.SetTrigger(tag);
        }

        public AnimatorProperty(Animator animator, string tag)
        {
            this.animator = animator;
            this.tag = tag;
        }
    }

    public class PlayerAnimatorProperties
    {
        private const int IDDLE_ANIMATION_VARIATION = 3;
        private readonly AnimatorProperty<bool> _isOverrided = null;
        private readonly AnimatorProperty<float> _velocity = null;
        private readonly AnimatorProperty _usePc = null;
        private readonly AnimatorProperty _useSwitch = null;
        private readonly AnimatorProperty<int> _iddleRandomizer = null;
        public bool IsOverrided { get => _isOverrided.Get(); set => _isOverrided.Set(value); }
        public float Velocity { get => _velocity.Get(); set => _velocity.Set(value); }
        public void UsePc() => _usePc.Set();
        public void UseSwitch() => _useSwitch.Set();
        public void RandomizeIddleAnimation() => _iddleRandomizer.Set(Mathf.RoundToInt(Random.Range(0, IDDLE_ANIMATION_VARIATION)));

        public PlayerAnimatorProperties(Animator animator)
        {
            _isOverrided = new AnimatorProperty<bool>(animator, "IsOverrided");
            _velocity = new AnimatorProperty<float>(animator, "Velocity");
            _usePc = new AnimatorProperty(animator, "UsePc");
            _useSwitch = new AnimatorProperty(animator, "UseSwitch");
            _iddleRandomizer = new AnimatorProperty<int>(animator, "Iddle_Randomizer");
        }
    }

    public partial class PlayerController
    {
        private Animator animator;
        private PlayerAnimatorProperties animatorProperties;

        public void PlayPickUpAnimation()
        {
            animatorProperties.IsOverrided = true;
            animator.SetLayerWeight(1, 1);
            animator.SetLayerWeight(2, 1);
        }

        public void PlayPutDownAnimation()
        {
            animatorProperties.IsOverrided = false;
            animator.SetLayerWeight(1, 0);
            animator.SetLayerWeight(2, 0);
        }

        // Animator handling

        internal void AnimatorUpdate(AnimatorValues values) => animatorProperties.Velocity = values.velocity;


        public void ApplyUsePc() => animatorProperties.UsePc();

        public void ApplyUseSwitch() => animatorProperties.UseSwitch();

        //Animator event methods

        public void ChangeIddleAniamtion() => animatorProperties.RandomizeIddleAnimation();
    }
}
