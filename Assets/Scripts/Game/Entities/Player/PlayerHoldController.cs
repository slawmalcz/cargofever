﻿using Game.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Entities
{
    public partial class PlayerController
    {
        public ICharacterHoldInteractive CurrentHoldedObject { get; private set; }
        private bool IsHolding => CurrentHoldedObject != null;
        private List<ICharacterHoldInteractive> HoldInteractiveObjects;

        private void AddHoldInteractiveObject(ICharacterHoldInteractive interactive)
        {
            if (interactive == null || HoldInteractiveObjects.Contains(interactive))
                return;
            HoldInteractiveObjects.Add(interactive);
        }

        private void RemoveHoldInteractiveObject(ICharacterHoldInteractive interactive)
        {
            if (interactive == null || !HoldInteractiveObjects.Contains(interactive))
                return;
            HoldInteractiveObjects.Remove(interactive);
        }

        public bool IsFacingInteraction(ICharacterHoldInteractive interactiveObject)
        {
            if (interactiveObject == null)
                return false;
            var facingVector = new Vector2(transform.forward.x, transform.forward.z);
            var interactionVector = new Vector2(
                interactiveObject.AttachedGameObject.transform.position.x - transform.position.x,
                interactiveObject.AttachedGameObject.transform.position.z - transform.position.z);
            return Vector2.Angle(facingVector, interactionVector) < INTERACTION_MAX_ANGLE;
        }

        private void HoldInteract()
        {
            if (IsHolding)
            {
                HoldInteractStop();
            }
            else
            {
                HoldInteractStart();
            }
        }

        private void HoldInteractStart()
        {
            HoldInteractiveObjects = HoldInteractiveObjects.Where(x => !(x is null) && ((x?.AttachedGameObject ?? null) != null)).ToList();
            var orderedInteractionList = HoldInteractiveObjects.OrderBy(x => Vector3.Distance(transform.position, x.AttachedGameObject.transform.position));
            foreach (var interactionObject in orderedInteractionList)
                if (IsFacingInteraction(interactionObject) && interactionObject.HoldInteractBegine(this))
                {
                    CurrentHoldedObject = interactionObject;
                    break;
                }
        }

        private void HoldInteractStop()
        {
            CurrentHoldedObject?.HoldInteractEnd(this);
            CurrentHoldedObject = null;
        }
    }
}
