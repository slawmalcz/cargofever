﻿using Game.Data;
using Game.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Entities
{
    public partial class PlayerController
    {
        private List<ICharacterInteractive> InteractiveObjects;

        private void AddInteractiveObject(ICharacterInteractive interactive)
        {
            if (interactive == null || InteractiveObjects.Contains(interactive))
                return;
            InteractiveObjects.Add(interactive);
        }

        private void RemoveInteractiveObject(ICharacterInteractive interactive)
        {
            if (interactive == null || !InteractiveObjects.Contains(interactive))
                return;
            InteractiveObjects.Remove(interactive);
        }

        public bool IsFacingInteraction(ICharacterInteractive interactiveObject)
        {
            if (interactiveObject == null)
                return false;
            var facingVector = new Vector2(transform.forward.x, transform.forward.z);
            var interactionVector = new Vector2(
                interactiveObject.AttachedGameObject.transform.position.x - transform.position.x,
                interactiveObject.AttachedGameObject.transform.position.z - transform.position.z);
            return Vector2.Angle(facingVector, interactionVector) < INTERACTION_MAX_ANGLE;
        }

        private void Interact()
        {
            InteractiveObjects = InteractiveObjects.Where(x => !(x is null) && ((x?.AttachedGameObject ?? null) != null)).ToList();
            var orderedInteractionList = InteractiveObjects.OrderBy(x => Vector3.Distance(transform.position, x.AttachedGameObject.transform.position));
            foreach (var interactionObject in orderedInteractionList)
                if (interactionObject != null && IsFacingInteraction(interactionObject) && interactionObject.Interact(this))
                    break;
        }

        public void PickUp(Item item)
        {
            cariedObject = item;
            Instantiate(cariedObject.prefab, carriedObjectTransform);
            PlayPickUpAnimation();
        }

        public Item PutDown()
        {
            var ret = cariedObject;
            Destroy(carriedObjectTransform.GetChild(0).gameObject);
            PlayPutDownAnimation();
            cariedObject = null;
            return ret;
        }
    }
}
