﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Entities
{
    public class PlayerDriver : MonoBehaviour
    {
        private const float INPUT_ACTIVATION_TRESHOLD = 0.05f;

        private PlayerController playerController = null;
        private Rigidbody playerRigidbody = null;

        [Header("Moment parameters")]
        public float speed = 2;
        public float maxSpeed = 3;

        public bool IsRotationLocked { get; set; }

        private Vector2 fowardVector = Vector2.zero;
        private Quaternion startingRotation = Quaternion.identity;

        private bool isStoped = false;
        private Quaternion frozenRoation;

        public void Start()
        {
            fowardVector = new Vector2(transform.forward.x, transform.forward.z);
            startingRotation = transform.rotation;
            playerController = gameObject.GetComponent<PlayerController>();
            playerRigidbody = playerController.playerRigidbody;
        }

        public void Update()
        {
            var values = HandleInput(playerController.HaveControll);
            playerController.AnimatorUpdate(values);
        }

        private AnimatorValues HandleInput(bool haveControll = true)
        {
            var rawInputVector = (haveControll) ? new Vector2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) : Vector2.zero;

            if (IsRotationLocked)
            {
                isStoped = false;
                var angularVelocity = 0;
                var velocity = ApplyLockedVelocity();
                return new AnimatorValues(angularVelocity, velocity);
            }
            else if (rawInputVector.magnitude < INPUT_ACTIVATION_TRESHOLD)
            {
                if (isStoped)
                {
                    transform.rotation = frozenRoation;
                }
                else
                {
                    frozenRoation = transform.rotation;
                    isStoped = true;
                }

                return new AnimatorValues(0, 0);
            }
            else
            {
                isStoped = false;
                var redefinedInputVector = rawInputVector.x * fowardVector.normalized + rawInputVector.y * -Vector2.Perpendicular(fowardVector).normalized;
                var angularVelocity = ApplyRotation(-Vector2.SignedAngle(fowardVector, redefinedInputVector));
                var velocity = ApplyVelocity();
                return new AnimatorValues(angularVelocity, velocity);
            }
        }

        private float ApplyRotation(float desiredRotation)
        {
            var desiredQuat = startingRotation * Quaternion.Euler(0, desiredRotation, 0);
            var angilarVelocity = transform.rotation;
            transform.rotation = desiredQuat;
            return Quaternion.Dot(angilarVelocity, transform.rotation);
        }

        private float ApplyVelocity()
        {
            var newVelocity = (transform.forward + new Vector3(0, 0.4f, 0)) * Time.deltaTime * speed;
            playerRigidbody.velocity += newVelocity;
            if (playerRigidbody.velocity.magnitude > maxSpeed)
                playerRigidbody.velocity = playerRigidbody.velocity.normalized * maxSpeed;
            return playerRigidbody.velocity.magnitude / maxSpeed;
        }

        private float ApplyLockedVelocity()
        {
            var newVelocity = (-transform.forward + new Vector3(0, 0.4f, 0)) * Time.deltaTime * speed;
            playerRigidbody.velocity += newVelocity;


            if (playerRigidbody.velocity.magnitude > maxSpeed)
                playerRigidbody.velocity = playerRigidbody.velocity.normalized * maxSpeed;
            return playerRigidbody.velocity.magnitude / maxSpeed;
        }
    }
}

