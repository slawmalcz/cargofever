﻿using Game.Data;
using Game.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Entities
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(PlayerDriver))]
    public partial class PlayerController : AbstractCharacterController
    {
        private const float INTERACTION_MAX_ANGLE = 70f;

        [Header("Required components")]
        public WorldController worldController = null;
        public Transform carriedObjectTransform = null;
        public PlayerDriver playerDriver = null;

        internal Item cariedObject = null;
        public bool IsCarryObject => !(cariedObject is null);

        protected override void Initialize()
        {
            //PlayerAnimatorController
            animator = GetComponent<Animator>();
            animatorProperties = new PlayerAnimatorProperties(animator);
            //PlayerInteractionController
            InteractiveObjects = new List<ICharacterInteractive>();
            HoldInteractiveObjects = new List<ICharacterHoldInteractive>();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (worldController.IsGamePaused)
                {
                    worldController.UnpauseGame();
                    HaveControll = true;
                }
                else
                {
                    worldController.PauseGame();
                    HaveControll = false;
                }
            }
            if (!HaveControll)
                return;
            if (Input.GetKeyDown(KeyCode.Space))
                Interact();
            if (Input.GetKeyDown(KeyCode.E))
                HoldInteractStart();
            if (Input.GetKeyUp(KeyCode.E))
                HoldInteractStop();

        }

        public void ApplyInteractive(GameObject detector, bool isEntering)
        {
            if (isEntering)
            {
                AddInteractiveObject(detector.GetComponent<ICharacterInteractive>());
                AddHoldInteractiveObject(detector.GetComponent<ICharacterHoldInteractive>());
            }
            else
            {
                RemoveInteractiveObject(detector.GetComponent<ICharacterInteractive>());
                RemoveHoldInteractiveObject(detector.GetComponent<ICharacterHoldInteractive>());
            }
        }
    }
}

