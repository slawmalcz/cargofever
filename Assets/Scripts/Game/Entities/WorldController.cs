﻿using CampainMap;
using Game.Ui;
using MainMenu.Data;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Entities
{
    public class WorldController : MonoBehaviour
    {
        public static WorldController Instance { get; private set; }
        public bool IsGamePaused { get; internal set; } = false;

        public PlayerController player = null;
        public int points = 0;
        [Header("Ui options")]
        public MainTimerUiController mainTimerUiController = null;
        public OrdersUiController ordersUiController = null;
        public GameObject quickMenuUi = null;
        [Header("Scene management")]
        public string campainLevelName = null;
        public string mainMenuLevelName = null;

        public void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Debug.LogError("Multiple WorldControllers in Scene");

            var info = ResourcesController.Instance.GetLevelInfo(SceneManager.GetActiveScene().name);
            mainTimerUiController.Init(info.levelTime, OnRoundEnd);
        }

        public void UnpauseGame()
        {
            quickMenuUi.SetActive(false);
            IsGamePaused = false;
            mainTimerUiController.StartTimer();
            Time.timeScale = 1;
        }
        public void PauseGame()
        {
            IsGamePaused = true; 
            mainTimerUiController.StopTimer();
            Time.timeScale = 0;
            quickMenuUi.SetActive(true);
        }

        public void ExitToCampainMap()
        {
            Time.timeScale = 1;
            StartCoroutine(TransferToCampainMap(0));
        }

        public void ExitToMainMenu()
        {
            Time.timeScale = 1;
            var campainController = FindObjectOfType<CampainController>().gameObject;
            Destroy(campainController);
            SceneManager.LoadScene(mainMenuLevelName, LoadSceneMode.Single);
        }


        private void OnRoundEnd()
        {
            player.HaveControll = false;
            var campainController = FindObjectOfType<CampainController>();
            if (campainController == null)
                Debug.LogError("Cant end round during shut down phase");
            campainController.SaveData(points);
            StartCoroutine(TransferToCampainMap(5));
        }

        public IEnumerator TransferToCampainMap(float time)
        {
            var campainController = FindObjectOfType<CampainController>();
            var dataPasser = new GameObject("DataPasser");
            var loadDataPasser = dataPasser.AddComponent<LoadDataPasser>();
            loadDataPasser.save = campainController.loadedSave;
            loadDataPasser.saveSlotName = campainController.saveSlotName;
            loadDataPasser.levelName = SceneManager.GetActiveScene().name;
            Destroy(campainController.gameObject);
            DontDestroyOnLoad(dataPasser);
            yield return new WaitForSeconds(time);
            SceneManager.LoadScene(campainLevelName, LoadSceneMode.Single);
        }
    }
}