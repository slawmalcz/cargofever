﻿using Game.Utilities;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Entities
{
    public class ForkLiftController : AbstractCharacterController, ICharacterInteractive
    {
        public CharacterInteractiveController interactiveController;
        [Header("Fork properties")]
        public Transform palletSlot = null;
        public Transform forkTransform = null;
        [Space(10)]
        public Transform forkUpPosition = null;
        public Transform forkDownPosition = null;
        [Space (10)]
        public AnimationCurve forkTransitionCurve = null;
        public float transferTime = 1;
        [Space(10)]
        public AcitonWorkFlow acitonWorkFlow = AcitonWorkFlow.Unloaded;

        [Header("Lift properties")]
        public Transform playerSlot = null;
        public GameObject playerModelPrefab = null;
        private GameObject playerInstance = null;
        [Space(10)]
        public AbstractDriverController driveController = null;

        private Coroutine currentCorutine = null;
        public GameObject carridePallet = null;
        public bool IsPalletSlotFree => carridePallet == null;

        public IForkLiftInteractive InteractiveObject { get; set; } = null;
        public GameObject AttachedGameObject => gameObject;


        protected override void Initialize()
        {
            OnHaveControllChange += (bool oldValue, bool newValue) => { driveController.isActive = newValue; };
            driveController.isActive = HaveControll;
        }

        void Update()
        {
            if (!HaveControll)
                return;
            if (Input.GetKeyDown(KeyCode.Space))
                PerformAction();
            if (Input.GetKeyDown(KeyCode.E))
                ExitVehicle();
        }

        private void PerformAction()
        {
            if (currentCorutine != null)
                StopCoroutine(currentCorutine);
            switch (acitonWorkFlow)
            {
                case AcitonWorkFlow.Loaded:
                    DropPallet();
                    currentCorutine = StartCoroutine(TransferForkLift(forkDownPosition, transferTime));
                    break;
                case AcitonWorkFlow.Unloaded:
                    LoadPallet();
                    acitonWorkFlow = AcitonWorkFlow.Loaded;
                    currentCorutine = StartCoroutine(TransferForkLift(forkUpPosition, transferTime));
                    break;
                default:
                    throw new NotImplementedException($"Fork lift can't handle action {acitonWorkFlow.ToString()}");
            }
        }

        private IEnumerator TransferForkLift(Transform destination, float timeToReachTarget, Action OnDestinationReachAction = null)
        {
            float t = 0;
            while (t < timeToReachTarget)
            {
                t += Time.fixedDeltaTime / timeToReachTarget;
                forkTransitionCurve.Evaluate(t);
                forkTransform.localPosition = Vector3.Lerp(forkTransform.localPosition, destination.localPosition, forkTransitionCurve.Evaluate(t));
                yield return new WaitForFixedUpdate();
            }
            OnDestinationReachAction?.Invoke();
            currentCorutine = null;
        }

        private void DropPallet()
        {
            carridePallet = null;
            palletSlot.GetComponent<FixedJoint>().connectedBody = null;
            acitonWorkFlow = AcitonWorkFlow.Unloaded;
        }

        private void LoadPallet() => InteractiveObject?.Interact(this);

        private void ExitVehicle()
        {
            HaveControll = false;
            playerInstance.SetActive(true);
            playerInstance.transform.parent = null;
            playerInstance.GetComponent<PlayerController>().HaveControll = true;
            for (var i = 0; i < playerSlot.transform.childCount; i++)
                Destroy(playerSlot.transform.GetChild(i).gameObject);
        }

        // ---- ICharacterInteractive implementation ----

        public bool Interact(PlayerController player)
        {
            if (this == null || player.IsCarryObject)
                return false;
            HaveControll = true;
            player.HaveControll = false;
            playerInstance = player.gameObject;
            playerInstance.transform.parent = gameObject.transform;
            playerInstance.SetActive(false);
            Instantiate(playerModelPrefab, playerSlot.transform);
            return true;
        }
    }
}
