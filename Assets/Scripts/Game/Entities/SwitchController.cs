﻿using Game.Utilities;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Entities
{
    class SwitchController : MonoBehaviour, ICharacterInteractive, IForkLiftInteractive
    {
        public UnityEvent OnPressed = null;
        public Transform playerUsePosition = null;
        public bool isPlayerActivated = true;
        public bool isVehicleActivated = false;

        public GameObject AttachedGameObject => gameObject;

        public bool Interact(PlayerController player)
        {
            if (OnPressed == null || !isPlayerActivated)
                return false;
            ForcePosition(player);
            OnPressed.Invoke();
            player.ApplyUseSwitch();
            return true;
        }

        public void Interact(ForkLiftController forkLift)
        {
            if (OnPressed != null && isVehicleActivated)
                OnPressed.Invoke();
        }

        private void ForcePosition(PlayerController player)
        {
            player.transform.position = playerUsePosition.transform.position;
            player.transform.rotation = playerUsePosition.transform.rotation;
        }
    }
}
