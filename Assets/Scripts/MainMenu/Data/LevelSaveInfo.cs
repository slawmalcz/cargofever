﻿using System;

namespace MainMenu.Data
{
    [Serializable]
    public class LevelSaveInfo
    {
        public string sceneName = "";
        public string gameObjectName = "";
        public int maxScore = 0;
        public int aquiredStars = 0;

        public LevelSaveInfo() { }
        public LevelSaveInfo(string sceneName, string gameObjectName, int maxScore = 0,int aquiredStars = 0)
        {
            this.sceneName = sceneName;
            this.gameObjectName = gameObjectName;
            this.maxScore = maxScore;
            this.aquiredStars = aquiredStars;
        }
    }
}
