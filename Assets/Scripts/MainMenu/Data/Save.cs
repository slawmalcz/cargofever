﻿using Game.Data;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

namespace MainMenu.Data
{
    public class Save
    {
        private const string LEVEL_INFO_PATH = "Level_Info";

        public bool isFullyInitialized = false;

        public int NumberOfStars => LevelInfos?.Sum(x => x.aquiredStars)??0;

        public LevelSaveInfo[] LevelInfos {
            get => levelsinforamtion.ToArray();
            set => levelsinforamtion = value.ToList();
        }
        public LevelSaveInfo GetSaveInfo(string sceneName) => LevelInfos.FirstOrDefault(x => x.sceneName == sceneName);


        [XmlIgnore]
        public List<LevelSaveInfo> levelsinforamtion = null;

        public Save()
        {
            levelsinforamtion = new List<LevelSaveInfo>();
            var levelInfoObjects = Resources.LoadAll<LevelInfo>(LEVEL_INFO_PATH);
            foreach (var data in levelInfoObjects)
                levelsinforamtion.Add(new LevelSaveInfo(data.levelScene, ""));
        }

        public void Reinitialize(Dictionary<string,GameObject> levelsEntersInstances)
        {
            foreach(var levelEntrance in levelsEntersInstances)
            {
                var levelInfoEntry = LevelInfos.FirstOrDefault(x => x.sceneName == levelEntrance.Key);
                if (levelInfoEntry == null)
                    Debug.LogError($"Data specified in scene can't be saved. \n" +
                                   $"Save LevelInfos don't have info on levelScene {levelEntrance.Key} attached to GameObject {levelEntrance.Value.name}");
                levelInfoEntry.gameObjectName = levelEntrance.Value.name;
            }
            isFullyInitialized = true;
        }
    }
}