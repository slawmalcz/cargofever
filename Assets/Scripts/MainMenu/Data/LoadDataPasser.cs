﻿using UnityEngine;

namespace MainMenu.Data
{
    class LoadDataPasser : MonoBehaviour
    {
        public Save save = null;
        public string saveSlotName = null;
        public string levelName = null;
    }
}
