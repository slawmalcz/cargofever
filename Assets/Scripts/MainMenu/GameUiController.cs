﻿using MainMenu.Data;
using MainMenu.Entity;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainMenu.Ui
{

    public class GameUiController : MonoBehaviour
    {
        public string CampainSceneName = null;
        public string TutorialScene = null;
        public GameObject mainMenu = null;
        public GameObject loadTab = null;
        public GameObject optionsTab = null;
        public GameObject creditsTab = null;
        public GameObject newGamePopUp = null;

        internal void LoadGame()
        {
            var saveToLoad = SavesController.Instance.saveSlots.First().Key;
            LoadGame(saveToLoad);
        }

        internal void LoadGame(string slotName)
        {
            OptionsController.UpdateValue("last_played_save", slotName);
            var dataPasser = new GameObject("DataPasser");
            var loadDataPasser = dataPasser.AddComponent<LoadDataPasser>();
            loadDataPasser.save = SavesController.Instance.saveSlots[slotName];
            loadDataPasser.saveSlotName = slotName;
            DontDestroyOnLoad(dataPasser);
            SceneManager.LoadScene(CampainSceneName, LoadSceneMode.Single);
        }

        internal void StartTutorial() => SceneManager.LoadScene(TutorialScene, LoadSceneMode.Single);

        internal void NewGame() => newGamePopUp.SetActive(true);

        internal void EnableTab(MainMenuTabs loadGame)
        {
            DeactivateAll();
            switch (loadGame)
            {
                case MainMenuTabs.MainManu:
                    mainMenu.SetActive(true);
                    break;
                case MainMenuTabs.LoadGame:
                    if (SavesController.Instance.saveSlots.Count > 0)
                        loadTab.GetComponent<LoadTabUiController>().Init(SavesController.Instance.saveSlots.Keys.ToList());
                    loadTab.SetActive(true);
                    break;
                case MainMenuTabs.Options:
                    optionsTab.SetActive(true);
                    break;
                case MainMenuTabs.Credits:
                    creditsTab.SetActive(true);
                    break;
            }
        }

        private void DeactivateAll()
        {
            mainMenu.SetActive(false);
            loadTab.SetActive(false);
            optionsTab.SetActive(false);
            creditsTab.SetActive(false);
        }
    }
}
