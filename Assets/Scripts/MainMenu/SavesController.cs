﻿using MainMenu.Data;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MainMenu.Entity
{
    public class SavesController
    {
        public static string SavesPath => $"{Application.persistentDataPath}/Saves";

        public static SavesController Instance {
            get {
                if (_instance == null)
                    _instance = new SavesController();
                return _instance;
            }
        }
        private static SavesController _instance = null;

        public Dictionary<string, Save> saveSlots = null;

        // Body

        private SavesController()
        {
            saveSlots = new Dictionary<string, Save>();
            var files = Directory.GetFiles(SavesPath);
            foreach (var fileName in files)
            {
                var reader =
                    new System.Xml.Serialization.XmlSerializer(typeof(Save));
                var file = new StreamReader(fileName);
                var overview = (Save)reader.Deserialize(file);
                file.Close();
                saveSlots.Add(Path.GetFileNameWithoutExtension(fileName), overview);
            }
        }

        public static string CreatNewSave(string name = "Test0")
        {
            var save = new Save();
            return SaveSave(save, name);
        }

        public static string SaveSave(Save save, string name)
        {
            var overview = save;
            var writer = new System.Xml.Serialization.XmlSerializer(typeof(Save));

            var path = $"{SavesPath}/{name}.xml";
            if (!Directory.Exists(SavesPath))
                Directory.CreateDirectory(SavesPath);
            var file = File.Create(path);

            writer.Serialize(file, overview);
            file.Close();

            Instance.saveSlots[name] = save;
            return name;
        }

        public static void ReloadSaves()
        {
            _instance = new SavesController();
        }
    }
}