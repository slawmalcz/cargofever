﻿using MainMenu.Data;
using MainMenu.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Ui
{
    public class LoadTabUiController : MonoBehaviour
    {
        public GameUiController gameController = null;

        public GameObject loadingSlotPrefab = null;
        public GameObject contentTransform = null;

        public GameObject deleteSaveButton = null;
        public GameObject loadSaveButton = null;

        private Tuple<string, GameObject> selectedSlot = null;

        public Dictionary<string, GameObject> buttonsDictionary = null;

        public void Init(List<string> sloatsNames)
        {
            buttonsDictionary = new Dictionary<string, GameObject>();
            foreach (var name in sloatsNames)
                AddSaveSlot(name);
        }

        private Button.ButtonClickedEvent GetButtonPressedHandler(string tes, GameObject caller)
        {
            var test = new Button.ButtonClickedEvent();
            test.AddListener(new UnityEngine.Events.UnityAction(() => { HandleSlotSelection(tes, caller); }));
            return test;
        }

        private void AddSaveSlot(string name)
        {
            var newButton = Instantiate(loadingSlotPrefab, contentTransform.transform);
            newButton.GetComponentInChildren<Text>().text = name;
            newButton.GetComponent<Button>().onClick = GetButtonPressedHandler(name, newButton);
            buttonsDictionary.Add(name, newButton);
        }

        private void ClearContent()
        {
            for (var i = 0; i < contentTransform.transform.childCount; i++)
                Destroy(contentTransform.transform.GetChild(i).gameObject);
            buttonsDictionary = new Dictionary<string, GameObject>();
        }

        private void HandleSlotSelection(string slotName, GameObject caller)
        {
            if (!loadSaveButton.activeInHierarchy || !deleteSaveButton.activeInHierarchy)
            {
                loadSaveButton.SetActive(true);
                deleteSaveButton.SetActive(true);
            }

            if (selectedSlot != null)
            {
                var colorTeamplateBase = caller.GetComponent<Image>().color;
                selectedSlot.Item2.GetComponent<Image>().color = colorTeamplateBase;
            }

            selectedSlot = new Tuple<string, GameObject>(slotName, caller);

            selectedSlot.Item2.GetComponent<Image>().color = Color.blue;
        }

        // --- Button Handling ---
        public void OnBackButtonPressde()
        {
            ClearContent();
            gameController.EnableTab(MainMenuTabs.MainManu);
        }

        public void OnLoadButtonPressed()
        {
            ClearContent();
            gameController.LoadGame(selectedSlot.Item1);
        }

        public void OnDeleteButtolPressed()
        {
            var path = $"{SavesController.SavesPath}/{selectedSlot.Item1}.xml";
            File.Delete(path);
            SavesController.ReloadSaves();
            loadSaveButton.SetActive(false);
            deleteSaveButton.SetActive(false);
            Destroy(selectedSlot.Item2);
            selectedSlot = null;
        }
    }
}
