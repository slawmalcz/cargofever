﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.MainMenu
{
    class TraficController : MonoBehaviour
    {
        public GameObject[] traficGenerators = null;

        public float timeOfFirstGeneration = 0;
        public float maxDeleay = 5;

        public void Start()
        {
            foreach(var generator in traficGenerators)
            {
                StartCoroutine(StartingProcedure(generator, timeOfFirstGeneration + maxDeleay * UnityEngine.Random.value));
            }
        }

        private IEnumerator StartingProcedure(GameObject caller,float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            caller.SetActive(true);
        }
    }
}
