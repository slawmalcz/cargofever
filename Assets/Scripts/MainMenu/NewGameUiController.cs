﻿using MainMenu.Entity;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Ui
{
    public class NewGameUiController : MonoBehaviour
    {
        public GameUiController gameUiController = null;
        public InputField inputField = null;

        public void OnBackPressed()
        {
            inputField.text = "";
            gameObject.SetActive(false);
        }

        public void OnSavePressed()
        {
            var saveToReimport = SavesController.CreatNewSave($"{inputField.text}");
            gameUiController.LoadGame(saveToReimport);
        }
    }
}

