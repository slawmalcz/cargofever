﻿using MainMenu.Data;
using UnityEngine;

namespace MainMenu.Ui
{
    public class OptionsUiController : MonoBehaviour
    {
        public GameUiController gameController = null;

        public void OnBackPressed() => gameController.EnableTab(MainMenuTabs.MainManu);
    }
}
