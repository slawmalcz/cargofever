﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MainMenu.Entity
{
    public class OptionsController
    {
        public static string OPTIONS_FILE_PATH => $"{Application.persistentDataPath}/Options";
        public const string FILE_NAME = "config.txt";

        public static OptionsController Instance {
            get {
                if (_instance == null)
                    _instance = new OptionsController();
                return _instance;
            }
        }

        private static OptionsController _instance = null;

        //--- Body ---

        public Dictionary<string, string> options;
        private readonly Dictionary<string, int> optionLine;

        private void VersionControll()
        {
            if (!Directory.Exists(OPTIONS_FILE_PATH))
                Directory.CreateDirectory(OPTIONS_FILE_PATH);
            //Creat options file if non existion
            if (!File.Exists($"{OPTIONS_FILE_PATH}/{FILE_NAME}"))
            {
                var textAsset = Resources.Load<TextAsset>("DefaultOptions").text;
                File.WriteAllText($"{OPTIONS_FILE_PATH}/{FILE_NAME}", textAsset);
                return;
            }
            // Existing options file is outdated
            if (ExtractVersionNumber(Resources.Load<TextAsset>("DefaultOptions").text.Replace("\r","").Split("\n"[0])) !=
                ExtractVersionNumber(File.ReadAllLines($"{OPTIONS_FILE_PATH}/{FILE_NAME}")))
            {
                File.Delete($"{OPTIONS_FILE_PATH}/{FILE_NAME}");
                var textAsset = Resources.Load<TextAsset>("DefaultOptions").text;
                File.WriteAllText($"{OPTIONS_FILE_PATH}/{FILE_NAME}", textAsset);
                return;
            }
        }

        private string ExtractVersionNumber(string[] lines)
        {
            foreach (var line in lines)
            {
                //Comments handling
                if (line.StartsWith("##")) continue;
                var words = line.Split('=');
                var key = words[0].Trim(' ');
                var value = words[1].Substring(1);
                if (key == "version")
                    return value;
            }
            return "0.0.0";
        }

        private OptionsController()
        {
            options = new Dictionary<string, string>();
            optionLine = new Dictionary<string, int>();
            VersionControll();
            var textAsset = File.ReadAllLines($"{OPTIONS_FILE_PATH}/{FILE_NAME}");
            for (var i = 0; i < textAsset.Length; i++)
            {
                var line = textAsset[i];
                //Comments handling
                if (line.StartsWith("##")) continue;
                var words = line.Split('=');
                var key = words[0].Trim(' ');
                var value = words[1].Substring(1);
                if (!string.IsNullOrEmpty(key))
                {
                    options.Add(key, value);
                    optionLine.Add(key, i);
                }
            }
        }

        public static void UpdateValue(string key, string value)
        {
            var arrLine = File.ReadAllLines($"{OPTIONS_FILE_PATH}/{FILE_NAME}");
            arrLine[Instance.optionLine[key]] = $"{key} = {value}";
            Instance.options[key] = value;
            File.WriteAllLines($"{OPTIONS_FILE_PATH}/{FILE_NAME}", arrLine);
        }
    }
}