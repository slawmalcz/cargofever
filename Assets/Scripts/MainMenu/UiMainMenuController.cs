﻿using MainMenu.Data;
using MainMenu.Entity;
using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.Ui
{
    public class UiMainMenuController : MonoBehaviour
    {
        public GameUiController gameController = null;
        public Text versionDisplay = null;

        public GameObject continueButton = null;

        public void Start()
        {
            versionDisplay.text = $"Version: {OptionsController.Instance.options["version"]}";
            if (!string.IsNullOrEmpty(OptionsController.Instance.options["last_played_save"]) &&
                SavesController.Instance.saveSlots.ContainsKey(OptionsController.Instance.options["last_played_save"]))
                continueButton.SetActive(true);
        }

        // --- Button handling ---
        public void OnContinuePressed() => gameController.LoadGame(OptionsController.Instance.options["last_played_save"]);
        public void OnNewGamePressed() => gameController.NewGame();
        public void OnLoadGamePressed() => gameController.EnableTab(MainMenuTabs.LoadGame);
        public void OnOptionsPressed() => gameController.EnableTab(MainMenuTabs.Options);
        public void OnTutorialPressed() => gameController.StartTutorial();
        public void OnCreditsPressed() => gameController.EnableTab(MainMenuTabs.Credits);
        public void OnQuitPressed() => Application.Quit();
    }
}
