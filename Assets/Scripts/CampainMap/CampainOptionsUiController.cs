﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace CampainMap
{
    class CampainOptionsUiController : MonoBehaviour
    {
        public string mainMenuSceneName = null;

        public GameObject advancedOptionsTab = null;

        public CampainCharacterController campainCharacter = null;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                ReturnToGame();
        }

        public void OnOptionsButtonPressed()
        {
            advancedOptionsTab.SetActive(true);
            gameObject.SetActive(false);
        }

        public void OnExitToOsPressed() => Application.Quit();

        public void OnExitToMenuPressed()
        {
            Destroy(FindObjectOfType<CampainController>().gameObject);
            Time.timeScale = 1;
            SceneManager.LoadScene(mainMenuSceneName);
        }

        public void OnBackButtonPressed() => ReturnToGame();

        private void ReturnToGame()
        {
            Time.timeScale = 1;
            campainCharacter.HaveControll = true;
            gameObject.SetActive(false);
        }
    }
}
