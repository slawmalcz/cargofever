﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.CampainMap
{
    public class CatmullRomSpline : MonoBehaviour
    {
        //Has to be at least 4 points
        public Transform[] ControlPointsList {
            get {
                var childs = new List<Transform>();
                for (var i = 0; i < gameObject.transform.childCount; i++)
                    childs.Add(gameObject.transform.GetChild(i));
                return childs.ToArray();
            }
        }
        //Are we making a line or a loop?
        public bool isLooping = true;

        //Display without having to press play
        void OnDrawGizmos()
        {
            Gizmos.color = Color.white;

            //Draw the Catmull-Rom spline between the points
            for (var i = 0; i < ControlPointsList.Length; i++)
            {
                //Cant draw between the endpoints
                //Neither do we need to draw from the second to the last endpoint
                //...if we are not making a looping line
                if ((i == 0 || i == ControlPointsList.Length - 2 || i == ControlPointsList.Length - 1) && !isLooping)
                    continue;
                DisplayCatmullRomSpline(i);
            }
        }

        //Display a spline between 2 points derived with the Catmull-Rom spline algorithm
        void DisplayCatmullRomSpline(int pos)
        {
            //The 4 points we need to form a spline between p1 and p2
            var p0 = ControlPointsList[ClampListPos(pos - 1)].position;
            var p1 = ControlPointsList[pos].position;
            var p2 = ControlPointsList[ClampListPos(pos + 1)].position;
            var p3 = ControlPointsList[ClampListPos(pos + 2)].position;
            //The start position of the line
            var lastPos = p1;
            //The spline's resolution
            //Make sure it's is adding up to 1, so 0.3 will give a gap, but 0.2 will work
            var resolution = 0.2f;
            //How many times should we loop?
            var loops = Mathf.FloorToInt(1f / resolution);
            for (var i = 1; i <= loops; i++)
            {
                //Which t position are we at?
                var t = i * resolution;
                //Find the coordinate between the end points with a Catmull-Rom spline
                var newPos = GetCatmullRomPosition(t, p0, p1, p2, p3);
                //Draw this line segment
                Gizmos.DrawLine(lastPos, newPos);
                //Save this position so we can draw the next line segment
                lastPos = newPos;
            }
        }

        //Clamp the list positions to allow looping
        int ClampListPos(int pos)
        {
            if (pos < 0)
                pos = ControlPointsList.Length - 1;
            if (pos > ControlPointsList.Length)
                pos = 1;
            else if (pos > ControlPointsList.Length - 1)
                pos = 0;
            return pos;
        }

        //Returns a position between 4 Vector3 with Catmull-Rom spline algorithm
        // 0 <= t <= 1
        // t == 0 position == p1
        // t == 1 position = p2
        Vector3 GetCatmullRomPosition(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            //The coefficients of the cubic polynomial (except the 0.5f * which I added later for performance)
            var a = 2f * p1;
            var b = p2 - p0;
            var c = 2f * p0 - 5f * p1 + 4f * p2 - p3;
            var d = -p0 + 3f * p1 - 3f * p2 + p3;
            //The cubic polynomial: a + b * t + c * t^2 + d * t^3
            var pos = 0.5f * (a + (b * t) + (c * t * t) + (d * t * t * t));
            return pos;
        }

        Quaternion QuaternionInterpolation(float t, Quaternion p1, Quaternion p2) => Quaternion.Slerp(p1, p2, t);

        public void TravelSpineLine(GameObject spawner, GameObject prefab, GameObject caller, float spacing, float distance)
        {
            var currentPoints = new int[4] { 0, 1, 2, 3 };
            var canContinue = true;
            var tParameter = 0f;
            var traveledDistance = 0f;
            while (canContinue)
            {
                var currentPointPositions = new Vector3[4] {
                    ControlPointsList[currentPoints[0]].position,
                    ControlPointsList[currentPoints[1]].position,
                    ControlPointsList[currentPoints[2]].position,
                    ControlPointsList[currentPoints[3]].position
                };
                var currentPointRotations = new Quaternion[2] {
                    ControlPointsList[currentPoints[0]].rotation,
                    ControlPointsList[currentPoints[1]].rotation
                };
                traveledDistance += Vector3.Distance(
                    spawner.transform.position,
                    GetCatmullRomPosition(tParameter, currentPointPositions[0], currentPointPositions[1], currentPointPositions[2], currentPointPositions[3]));
                spawner.transform.position = GetCatmullRomPosition(tParameter, currentPointPositions[0], currentPointPositions[1], currentPointPositions[2], currentPointPositions[3]);
                spawner.transform.rotation = QuaternionInterpolation(tParameter, currentPointRotations[0], currentPointRotations[1]);
                if (traveledDistance > distance)
                {
                    var breadCrum = Instantiate(prefab);
                    breadCrum.transform.position = spawner.transform.position;
                    breadCrum.transform.rotation = spawner.transform.rotation;
                    breadCrum.transform.parent = caller.transform;
                }
                tParameter += spacing;
                if (tParameter > 1)
                {
                    var cycleToPerform = Mathf.FloorToInt(tParameter);
                    tParameter -= Mathf.FloorToInt(tParameter);
                    for (var i = 0; i < cycleToPerform; i++)
                    {
                        if (currentPoints[3] + 1 >= ControlPointsList.Length)
                        {
                            canContinue = false;
                            spawner.transform.position = ControlPointsList[ControlPointsList.Length - 2].position;
                            break;
                        }
                        else
                        {
                            currentPoints[0]++;
                            currentPoints[1]++;
                            currentPoints[2]++;
                            currentPoints[3]++;
                        }
                    }
                }
            }
        }
    }
}
