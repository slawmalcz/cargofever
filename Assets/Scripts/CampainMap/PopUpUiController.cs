﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CampainMap
{
    public class PopUpUiController : MonoBehaviour
    {
        public Text missionNameField = null;
        public Text maxScoreField = null;

        public GameObject[] starsSlots;

        internal void Init(string levelName, int maxScore, int numberOfStars)
        {
            missionNameField.text = levelName;
            maxScoreField.text = $"Max score: {maxScore.ToString()}";
            try
            {
                for (var i = 0; i < starsSlots.Length; i++)
                    if (i < numberOfStars)
                        starsSlots[i].SetActive(true);
                    else
                        starsSlots[i].SetActive(false);
            }
            catch (Exception ex)
            {
                Debug.LogError($"Level info of have difrent size of stars reward then {gameObject.name} PopUpController \n" +
                               $"{ex.Message}");
            }

        }
    }
}

