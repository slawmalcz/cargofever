﻿using UnityEngine;

namespace CampainMap
{
    class CampainAdvancedOptionsUi : MonoBehaviour
    {
        public GameObject mainTab = null;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Deactivate();
        }

        public void OnCanclePressed() => Deactivate();

        public void OnApplyPressed() => Deactivate();

        private void Deactivate()
        {
            mainTab.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
