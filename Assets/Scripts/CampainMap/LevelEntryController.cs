﻿using Game.Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CampainMap
{
    public class LevelEntryController : MonoBehaviour
    {
        public LevelInfo levelInfo = null;
        public GameObject popUpUi = null;
        public Animator pointAnimator = null;
        [Header("Detection options")]
        public string detectionTag = "Player";

        private PopUpUiController popUpUiController = null;

        public int NumberOfStars {
            get {
                var stars = 0;
                for (var i = 0; i < levelInfo.pointsToLevel.Length; i++)
                    if (levelInfo.pointsToLevel[i] < currentMaxScore)
                        stars++;
                    else
                        break;
                return stars;
            }
        }
        private int currentMaxScore = 0;

        public void Awake()
        {
            popUpUiController = popUpUi.GetComponent<PopUpUiController>();
            if (popUpUiController == null)
                Debug.LogError($"GameObject {gameObject.name} as {this.GetType().Name} don't have required element: " +
                               $"{typeof(PopUpUiController).ToString()} in GameObject {popUpUi.name}");
        }

        public void Activate() => SceneManager.LoadScene(levelInfo.levelScene, LoadSceneMode.Single);

        internal void LoadData(int maxScore,int currentPoints)
        {
            currentMaxScore = maxScore;
            gameObject.SetActive(levelInfo.requiredPoints <= currentPoints);
            popUpUiController.Init(levelInfo.levelName, currentMaxScore, NumberOfStars);
        }

        // --- Trigger Detectors ---

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(detectionTag))
            {
                other.GetComponent<CampainCharacterController>().currentSelectedController = this;
                pointAnimator.SetBool("IsActivated", true);
                popUpUi.SetActive(true);
            }

        }
        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(detectionTag))
            {
                popUpUi.SetActive(false);
                pointAnimator.SetBool("IsActivated", false);
                if (other.GetComponent<CampainCharacterController>().currentSelectedController == this)
                    other.GetComponent<CampainCharacterController>().currentSelectedController = null;
            }
        }
    }
}
