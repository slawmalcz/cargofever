﻿using Game.Entities;
using MainMenu.Data;
using MainMenu.Entity;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CampainMap
{
    public class CampainController : MonoBehaviour
    {
        public GameObject mainCharacter = null;

        public GameObject[] levelEntres = null;

        [HideInInspector]
        public Save loadedSave = null;
        [HideInInspector]
        public string saveSlotName = null;

        void Start()
        {
            var dataPassed = GameObject.Find("DataPasser");
            if (dataPassed == null)
                Debug.LogError("No DataPasser is enable on camping map start");
            loadedSave = dataPassed.GetComponent<LoadDataPasser>().save;
            saveSlotName = dataPassed.GetComponent<LoadDataPasser>().saveSlotName;
            if (!string.IsNullOrEmpty(dataPassed.GetComponent<LoadDataPasser>().levelName))
            {
                var lastPositionObject = levelEntres.Where(x => x.GetComponent<LevelEntryController>().levelInfo.levelScene == dataPassed.GetComponent<LoadDataPasser>().levelName).FirstOrDefault();
                mainCharacter.transform.position = lastPositionObject.transform.position;
            }
            DestroyImmediate(dataPassed);
            if (!loadedSave.isFullyInitialized)
            {
                loadedSave.Reinitialize(FindObjectsOfType<LevelEntryController>().ToDictionary(x => x.levelInfo.levelScene, y => y.gameObject));
                SavesController.SaveSave(loadedSave, saveSlotName);
            }
            LoadData(loadedSave);
            DontDestroyOnLoad(this);
        }

        private void LoadData(Save saveFile)
        {
            foreach (var levelInfo in saveFile.LevelInfos)
            {
                var handledGameObject = GameObject.Find(levelInfo.gameObjectName);
                handledGameObject.GetComponent<LevelEntryController>().LoadData(levelInfo.maxScore, saveFile.NumberOfStars);
            }
        }

        public void SaveData(int maxScore)
        {
            if (maxScore > loadedSave.GetSaveInfo(SceneManager.GetActiveScene().name).maxScore)
            {
                loadedSave.GetSaveInfo(SceneManager.GetActiveScene().name).maxScore = maxScore;
                loadedSave.GetSaveInfo(SceneManager.GetActiveScene().name).aquiredStars =
                    GetNumberOfStars(ResourcesController.Instance.GetLevelInfo(SceneManager.GetActiveScene().name).pointsToLevel, maxScore);
            }

            SavesController.SaveSave(loadedSave, saveSlotName);
        }

        private int GetNumberOfStars(int[] ponitToLevel, int score)
        {
            var stars = 0;
            for (var i = 0; i < ponitToLevel.Length; i++)
                if (ponitToLevel[i] < score)
                    stars++;
                else
                    break;
            return stars;
        }
    }
}
