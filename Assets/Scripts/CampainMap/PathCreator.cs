﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.CampainMap
{
    class PathCreator : MonoBehaviour
    {
        public GameObject breadcrumsPrefab = null;
        public CatmullRomSpline catmullRomSpline = null;

        public float spacing = 0.5f;
        public float detail = 0.1f;
        public void Start()
        {
            var spawner = new GameObject("Spawner");
            spawner.transform.parent = gameObject.transform;
            spawner.transform.position = catmullRomSpline.ControlPointsList.First().position;
            catmullRomSpline.TravelSpineLine(spawner, breadcrumsPrefab, gameObject, detail, spacing);
        }
    }
}
