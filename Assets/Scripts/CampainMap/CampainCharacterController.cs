﻿using Game.Utilities;
using System;
using UnityEngine;

namespace CampainMap
{
    public class CampainCharacterController : MonoBehaviour
    {
        public bool HaveControll {
            get => _haveControll;
            set {
                OnHaveControllChange.Invoke(_haveControll, value);
                _haveControll = value;
            }
        }
        public bool _haveControll = false;
        private event Action<bool, bool> OnHaveControllChange = delegate { };
        [Space(10)]
        public GameObject campainMainOptionsTab = null;
        [Space(10)]
        public SimpleDriveController driveController = null;

        [HideInInspector]
        public LevelEntryController currentSelectedController = null;

        public void Start()
        {
            OnHaveControllChange += (bool oldValue, bool newValue) => { driveController.isActive = newValue; };
            HaveControll = _haveControll;
        }

        void Update()
        {
            if (!HaveControll)
                return;
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                campainMainOptionsTab.SetActive(true);
                HaveControll = false;
                Time.timeScale = 0;
                return;
            }
            if (Input.GetKeyDown(KeyCode.Space))
                PerformAction();
        }

        private void PerformAction()
        {
            if (currentSelectedController != null)
                currentSelectedController.Activate();
        }
    }
}
