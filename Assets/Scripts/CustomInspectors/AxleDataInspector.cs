﻿using Game.Utilities;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CustomInspectors
{
    [CustomEditor(typeof(AbstractDriverController), true)]
    [CanEditMultipleObjects]
    class AbstractDriverControllerInspector : Editor
    {
        public SerializedProperty axels;
        private bool expandAxelsPad = false;
        private List<bool> extendedValues;

        void OnEnable()
        {
            axels = serializedObject.FindProperty("axles");
            extendedValues = new List<bool>();
            for(var i = 0; i < axels.arraySize; i++)
                extendedValues.Add(false);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();

            expandAxelsPad = EditorGUILayout.Foldout(expandAxelsPad, "Axels");
            if (expandAxelsPad)
            {
                EditorGUILayout.BeginVertical();
                EditorGUI.indentLevel++;
                DecomposeList(axels);

                EditorGUILayout.BeginHorizontal();
                if (GUILayout.Button("Add"))
                {
                    axels.arraySize++;
                    extendedValues.Add(false);
                }
                if (GUILayout.Button("Remove"))
                {
                    axels.arraySize--;
                }
                EditorGUILayout.EndHorizontal();

                EditorGUI.indentLevel--;
                EditorGUILayout.EndVertical();
            }
            
            serializedObject.ApplyModifiedProperties();
        }

        private void DecomposeList(SerializedProperty axels)
        {
            for (var i = 0; i < axels.arraySize; i++)
            {
                extendedValues[i] = EditorGUILayout.Foldout(extendedValues[i], $"Axle {i + 1}");
                if (extendedValues[i])
                {
                    var axle = axels.GetArrayElementAtIndex(i);
                    var leftWheel = axle.FindPropertyRelative("leftWheel");
                    var leftWheelModel = axle.FindPropertyRelative("leftWheelModel");
                    var rightWheel = axle.FindPropertyRelative("rightWheel");
                    var rightWheelModel = axle.FindPropertyRelative("rightWheelModel");
                    var motor = axle.FindPropertyRelative("motor");
                    var steering = axle.FindPropertyRelative("steering");

                    EditorGUILayout.BeginVertical();
                    EditorGUI.indentLevel++;

                    EditorGUILayout.LabelField("Left wheel");
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(leftWheel, new GUIContent());
                    EditorGUILayout.PropertyField(leftWheelModel, new GUIContent());
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.LabelField("Right wheel");
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(rightWheel, new GUIContent());
                    EditorGUILayout.PropertyField(rightWheelModel, new GUIContent());
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PropertyField(motor);
                    EditorGUILayout.PropertyField(steering);
                    EditorGUILayout.EndHorizontal();

                    EditorGUI.indentLevel--;
                    EditorGUILayout.EndVertical();
                }
                
            }
        }
    }
}
